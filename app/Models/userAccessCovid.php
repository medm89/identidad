<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class userAccessCovid extends Model
{
    protected $table = 'access_user_covid';
    protected $primaryKey = 'id';
    public $incrementing = true;
    protected $fillable = ['alias','access','create_at'];

}
