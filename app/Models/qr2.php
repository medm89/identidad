<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class qr2 extends Model
{
    protected $table = 'qr2';
    protected $primaryKey = 'id';
    public $incrementing = true;
    protected $fillable = ['id','code','token','timestamp','secret'];
}
