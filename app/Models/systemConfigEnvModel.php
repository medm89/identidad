<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class systemConfigEnvModel extends Model
{
    protected $table = 'system_config_env';
    protected $primaryKey = 'id';
    public $incrementing = true;
    protected $fillable = ['id','options','time','minVersionCode'];
}
