<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class raspberry extends Model
{
    protected $table = 'raspberry';
    protected $primaryKey = 'id';
    public $incrementing = true;
    protected $fillable = ['id','code','name'];
}
