<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class systemConfigModel extends Model
{
    protected $table = 'system_config';
    protected $primaryKey = 'id';
    public $incrementing = true;
    protected $fillable = ['id','options'];
}
