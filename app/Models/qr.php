<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class qr extends Model
{
    protected $table = 'qr';
    protected $primaryKey = 'id';
    public $incrementing = true;
    protected $fillable = ['id','code','token','timestamp','secret','flag'];
}
