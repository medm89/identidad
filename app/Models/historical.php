<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class user_historical extends Model
{
    protected $table = 'historical';
    protected $primaryKey = 'hist_id';
    protected $fillable = ['hist_id','hist_code','hist_uid','hist_phone_model','hist_phone_brand','hist_date_unsubscribe'];
}
