<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class userAccess extends Model
{
    protected $table = 'user_access';
    protected $primaryKey = 'id';
    public $incrementing = true;
    protected $fillable = ['ua_usuario','ua_dispositivo','ua_timestands','create_at'];

}
