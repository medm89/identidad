<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class perfil extends Model
{
    protected $table = 'perfil';
    protected $primaryKey = 'id';
    public $incrementing = true;
    protected $fillable = ['id','name','color','carnet'];
}
