<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class adminModel extends Model
{
    protected $table = 'admin';
    protected $primaryKey = 'id';
    public $incrementing = true;
    protected $fillable = ['email','status'];
}
