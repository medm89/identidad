<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class accessCovid extends Model
{
    protected $table = 'access_covid';
    protected $primaryKey = 'id';
    public $incrementing = true;
    protected $fillable = ['options','create_at'];

}
