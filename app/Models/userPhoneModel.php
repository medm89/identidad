<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class userPhoneModel extends Model
{
    protected $table = 'user_phone';
    protected $primaryKey = 'user_id';
    public $incrementing = true;
    protected $fillable = ['user_uid','user_code','user_email_un','user_email_ext','user_phone_model','user_phone_brand','user_phone_movilso','user_phone_versionso','user_last_access'];

}
