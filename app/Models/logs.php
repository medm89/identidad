<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class logs extends Model
{
    protected $table = 'logs';
    protected $primaryKey = 'id';
    public $incrementing = true;
    protected $fillable = ['id','code','action','message','systemMessage'];
}
