<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
               //Verifica si el metodo http es OPTIONS
               if ($request->isMethod('OPTIONS'))
               // Responde con un status 200
               $response = response('', 200);
           else
               // Pasa al siguiente middleware
               $response = $next($request);
   
           // agrega a la respuesta el acceso permitido 
           $response->header('Access-Control-Allow-Methods', '*');
           $response->header('Access-Control-Allow-Headers', $request->header('*'));
           $response->header('Access-Control-Allow-Origin', '*');
           $response->header('Access-Control-Expose-Headers', '*');
   
           return $response;
    }
}
