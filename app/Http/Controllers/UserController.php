<?php

namespace App\Http\Controllers;

use Crypt;
use File;
use App\User;
use App\Models\adminModel;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Models\userPhoneModel;
use App\Models\raspberry;
use App\Models\logs;
use App\Models\qr;
use App\Models\qr2;
use App\Models\userAccessCovid;
use App\Models\accessCovid;
use App\Models\userAccess;
use Validator, DB, Hash, Mail;
use App\Classes\FormatResponse;
use Carbon\Carbon;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Http\Controllers\PermissionCCureController;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;




class UserController extends FormatResponse
{
    public function  __construct(){
        $this->middleware('cors');
        $this->middleware('jwt');
    }

    public function getInfoUserByUserName(Request $request){
        $tokenApp = $request->headers->all();
        
        if(array_key_exists('tokenapp', $tokenApp)){
            $secretToken = config('app.secretToken');
            if($tokenApp['tokenapp'][0] == $secretToken ){

                $rules = [
                    'code'       => 'required'
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails()) {
                    return response()->json(['status'=> $this->estadoParametrosIncorrectos(), 'error'=> $validator->messages()]);
                }

                $user = userPhoneModel::select('user_code', 'user_email_un', 'user_email_ext', 'user_phone_model', 'user_phone_brand', 'user_phone_movilso', 'user_phone_versionso','created_at')
                ->where('user_email_un','like','%'.$request->code.'%')
                ->orwhere('user_email_ext','like','%'.$request->code.'%')
                ->first();

                 if($user){
                    $result = (new PermissionCCureController)->getUserPermissionIn($user->user_code);
                    $data   = $user;
                    $user2 = User::Where('code',$user->user_code)->first();
                    
                    if($user2){
                        return response()->json(['status'=> $this->estadoExitoso(), 'data'=>  $result, 'user_email'=>$user2->email ,'movil'=>$data ]);
                    }else{
                       return response()->json(['status'=> $this->estadoExitoso(), 'data'=>  $result, 'user_email'=>null ,'movil'=>$data ]);
                    }
                }
                
            }
            return $this->toJson($this->estadoNoAutorizado("Token de app no valido"));
        }
        return $this->toJson($this->estadoNoAutorizado());

    }

    public function getInfoUser(Request $request){
        $tokenApp = $request->headers->all();
        
        if(array_key_exists('tokenapp', $tokenApp)){
            $secretToken = config('app.secretToken');
            if($tokenApp['tokenapp'][0] == $secretToken ){

                $rules = [
                    'code'       => 'required'
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails()) {
                    return response()->json(['status'=> $this->estadoParametrosIncorrectos(), 'error'=> $validator->messages()]);
                }
                $result = (new PermissionCCureController)->getUserPermissionIn($request->code);
                $data   = userPhoneModel::where('user_code',$request->code)->first();
                $user = User::Where('code',$request->code)->first();
                 if($user){
                    return response()->json(['status'=> $this->estadoExitoso(), 'data'=>  $result, 'user_email'=>$user->email ,'movil'=>$data ]);
                 }else{
                    return response()->json(['status'=> $this->estadoExitoso(), 'data'=>  $result, 'user_email'=>null ,'movil'=>$data ]);
                 }
                
            }
            return $this->toJson($this->estadoNoAutorizado("Token de app no valido"));
        }
        return $this->toJson($this->estadoNoAutorizado());
        
    }



    public function encodeQR(Request $request){
        $tokenApp = $request->headers->all();
        if(array_key_exists('tokenapp', $tokenApp)){
            $secretToken = config('app.secretToken');
            if($tokenApp['tokenapp'][0] == $secretToken ){

                $rules = [
                    'code'        => 'required',
                    'timestamp'   => 'required'
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails()) {
                    return response()->json(['status'=> $this->estadoParametrosIncorrectos(), 'error'=> $validator->messages()]);
                }
                //$device = Crypt::encrypt($request->timestamp);
                $array = [8732, 12342, 4543, 43434, 342345,3433,76675,234244,43244,76767,1231,5452,4545];
                $device = str_replace('1','j',$request->timestamp);
                $device = str_replace('2', 'Z',$device);
                $device = str_replace('3', 'E',$device);
                $device = str_replace('4', 'A',$device);
                $device = str_replace('5', 'S',$device);
                $device = str_replace('6', 'g',$device);
                $device = str_replace('7', 'T',$device);
                $device = str_replace('8', 'b',$device);
                $device = str_replace('9', 'p',$device);
                $device = $device.$array[array_rand($array)];
                $user = qr::where('code',$request->code)->first();
                if($user){
                    $user->token      = $tokenApp['tokenapp'][0];
                    $user->timestamp  = $request->timestamp;
                    $user->flag       = 1;
                    $user->secret     = $device;
                    $user->save();
                }else{

                  qr::create(['code'=>$request->code,
                  'token'=>$tokenApp['tokenapp'][0],
                  'timestamp'=>$request->timestamp,
                  'flag'=>1,
                  'secret'=>$device]);
                }     
                      
                //return $this->toJson($this->estadoExitoso(),$device);
                $options = accessCovid::where('id',1)->first();
                $user = userPhoneModel::where('user_code',$request->code)->first();
                $alias = explode("@",$user->user_email_un)[0];
                $validar = userAccessCovid::where('alias',$alias)->first();

                $result = (new PermissionCCureController)->getUserPermissionIn($request->code);
                $array = $this->objectToJson($result);
                $contratista = false ;
                if( !empty($array) || $array['data']["perfiles"]!=""){
                    if (Str::contains($array['data']["perfiles"], 'CO')) {
                        $contratista = true;
                    }
                }
                if($contratista){
                    return response()->json(['status'=> $this->estadoExitoso(), 'data'=>$device  ]);
                }    
                

                if($options->options == "1"){
                    if($validar){
                        if($validar->access == 0){
                            return response()->json(['status'=> $this->estadoMedioExitoso("No puede ingresar al campus por los resultados registrados en la encuesta"), 'data'=>$device  ]);
                        }
                        if($validar->access == 1){
                            return response()->json(['status'=> $this->estadoExitoso(), 'data'=>$device  ]); 
                        }
                    }else{
                        return response()->json(['status'=> $this->estadoMedioExitoso("Debe diligenciar el reporte diario de condiciones de salud en la App Uninorte.co"), 'data'=>$device  ]);
                    }
                }
               return response()->json(['status'=> $this->estadoExitoso(), 'data'=>$device  ]);
            }
            return $this->toJson($this->estadoNoAutorizado("Token de app no valido"));
        }
        return $this->toJson($this->estadoNoAutorizado());
    }
    /**
	 * Desvincular dispositivo
     *
     * @authenticated
     * @group Dispositivo
     * @bodyParam $email string email del usuario
     * @response
     *
     * {
     *   "status": {
     *       "code": 1,
     *       "message": "Procesado con éxito"
     *       }
     *   }
     * @response
     * {
     *   "status": {
     *       "code": -1,
     *       "message": "Correo no encontrado"
     *       }
     *   }
	*/

    public function unlinkDevice(Request $request){
        $tokenApp = $request->headers->all();
        if(array_key_exists('tokenapp', $tokenApp)){
            $secretToken = config('app.secretToken');
            if($tokenApp['tokenapp'][0] == $secretToken ){

                // $rules = [
                //     'email'       => 'required:max:255'
                // ];
                // $validator = Validator::make($request->all(), $rules);
                // if($validator->fails()) {
                //     return response()->json(['status'=> $this->estadoParametrosIncorrectos(), 'error'=> $validator->messages()]);
                // }
                if($request->email == ""){
                    $device = userPhoneModel::Where('user_code',$request->code)
                    ->first();    
                }
                if($request->code == ''){
                    $device = userPhoneModel::where('user_email_un',$request->email)
                        ->orWhere('user_email_ext',$request->email)
                        ->orWhere('user_code',$request->code)
                        ->first();
                }
                
                if($device){
                    if(($device->user_email_ext && $device->user_email_un == null) || ($device->user_email_ext == null && $device->user_email_un)){
                        Log::info(' code '.$device->user_code.'action Desvinculo Dispositivo  message '.$request->email.' Desvinculo celular Marca '.$device->user_phone_model.' modelo '.$device->user_phone_brand);
                        // logs::create(['code'=>$device->user_code,'action'=>'Desvinculo Dispositivo','message'=>$request->email.' Desvinculo celular Marca '.$device->user_phone_model.' modelo '.$device->user_phone_brand,'systemMessage'=>2]);
                        $device->delete();
                    }
                    if($device->user_email_un && $device->user_email_ext){
                        if($device->user_email_un == $request->email){
                            $device->user_email_un = null;
                         }
                        if($device->user_email_ext == $request->email){
                            $device->user_email_ext = null;
                         }

                         Log::info(' code '.$device->user_code.' action Desvinculo Dispositivo  message '.$request->email.' Desvinculo celular Marca '.$device->user_phone_model.' modelo '.$device->user_phone_brand);
                        //  logs::create(['code'=>$device->user_code,'action'=>'Desvinculo Dispositivo','message'=>$request->email.' Desvinculo celular Marca '.$device->user_phone_model.' modelo '.$device->user_phone_brand,'systemMessage'=>2]);
                        $resp = $device;
                         $device->delete();
                    }
                    // return response()->json(['status'=> $this->estadoExitoso(), 'data'=>  $raspberry ]);
                    return $this->toJson($this->estadoExitoso());
                }
                return $this->toJson($this->estadoOperacionFallida('Correo no encontrado'));
            }
            return $this->toJson($this->estadoNoAutorizado("Token de app no valido"));
        }
        return $this->toJson($this->estadoNoAutorizado());
    }

    /**
        * Obtener dispositivos del usuario
        * @authenticated
        * @group Dispositivo
        * @bodyParam $email string email del usuario
        * @response
        * {
        *        "status": {
        *            "code": 1,
        *            "message": "Procesado con éxito"
        *        },
        *        "data": {
        *            "user_id": 3,
        *            "user_email_un": null,
        *            "user_email_ext": "jahel131296@gmail.com",
        *            "user_uid": "456789",
        *            "user_phone_model": null,
        *            "user_phone_brand": null,
        *            "user_last_access": null,
        *            "updated_at": "2020-03-05 22:34:39.023",
        *            "created_at": "2020-05-03 16:58:53.393"
        *        }
        *    }
        *
	*/
    public function getRegisterDevice(Request $request){
        $tokenApp = $request->headers->all();
        if(array_key_exists('tokenapp', $tokenApp)){
            $secretToken = config('app.secretToken');
            if($tokenApp['tokenapp'][0] == $secretToken ){

                $rules = [
                    'email'       => 'required:max:255'
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails()) {
                    return response()->json(['status'=> $this->estadoParametrosIncorrectos(), 'error'=> $validator->messages()]);
                }
                $device = userPhoneModel::where('user_email_un',$request->email)
                        ->orWhere('user_email_ext',$request->email)
                        ->first();
                return $this->toJson($this->estadoExitoso(),$device);
            }
            return $this->toJson($this->estadoNoAutorizado("Token de app no valido"));
        }
        return $this->toJson($this->estadoNoAutorizado());
    }
     /**
        * Obtener todos los dispositivo
        * @authenticated
        * @group Dispositivo
        * @bodyParam N/A
        * @response
        * {
        *        "status": {
        *            "code": 1,
        *            "message": "Procesado con éxito"
        *        },
        *        "data": {
        *            "user_id": 3,
        *            "user_email_un": null,
        *            "user_email_ext": "jahel131296@gmail.com",
        *            "user_uid": "456789",
        *            "user_phone_model": null,
        *            "user_phone_brand": null,
        *            "user_last_access": null,
        *            "updated_at": "2020-03-05 22:34:39.023",
        *            "created_at": "2020-05-03 16:58:53.393"
        *        }
        *    }
        *
	*/
    public function getAllRegisterDevice(Request $request){
        $tokenApp = $request->headers->all();
        if(array_key_exists('tokenapp', $tokenApp)){
            $secretToken = config('app.secretToken');
            if($tokenApp['tokenapp'][0] == $secretToken ){
                $device = userPhoneModel::all();
                return $this->toJson($this->estadoExitoso(),$device);
            }
            return $this->toJson($this->estadoNoAutorizado("Token de app no valido"));
        }
        return $this->toJson($this->estadoNoAutorizado());
    }

    /**
        * Subir foto del usuario
        * @authenticated
        * @bodyParam $code int cedula de usuario
        * @bodyParam $image file imagen de perfil
        *
        * @group Administración de usuario
        * @response {
        *"status": {
        *    "code": 1,
        *    "message": "Procesado con éxito"
        *},
        *"data": {
        *    "state": "",
        *    "response": 500,
        *    "error": "Archivo Incorrecto",
        *    "message": "ERROR"
        *}
        *}
        * @response {
        *        "status": {
        *    "code": 1,
        *    "message": "Procesado con éxito"
        *},
        *"data": {
        *    "response": 200,
        *    "message": "OK",
        *    "state": {
        *        "DO": {
        *            "id": 9030,
        *            "perfil": "DO",
        *            "message": "OK"
        *        },
        *        "EG": {
        *            "id": 9031,
        *            "perfil": "EG",
        *            "message": "OK"
        *        }
        *    }
        *}
        *}
        * @response
        *    {
        *    "status": {
        *        "code": 1,
        *        "message": "Procesado con éxito"
        *    },
        *    "data": {
        *        "response": 200,
        *        "message": "OK",
        *        "state": {
        *            "DO": {
        *                "message": "ERROR",
        *                "error": "Ya hay una solicitud creada",
        *                "perfil": "DO"
        *            },
        *            "EG": {
        *                "message": "ERROR",
        *                "error": "Ya hay una solicitud creada",
        *                "perfil": "EG"
        *            }
        *        }
        *    }
        *}
        *@response

        *{
        *    "status": {
        *        "code": -1,
        *        "message": "No se selecciono una foto"
        *    }
        *}
        *
        *@response
        *{
        *    "status": {
        *        "code": -1,
        *        "message": "Error: Operación fallida"
        *    }
        *}
	*/
    public function uploadPhoto(Request $request){
        $tokenApp = $request->headers->all();
        if(array_key_exists('tokenapp', $tokenApp)){
            $secretToken = config('app.secretToken');
            if($tokenApp['tokenapp'][0] == $secretToken ){
                $rules = [
                    'code'       => 'max:255',
                    'image' => 'image|mimes:jpg,png,svg,gif,jpeg|max:6144'
                ];
                $validator = Validator::make($request->all(), $rules);

                if($validator->fails()) {
                    return response()->json(['status'=> $this->estadoParametrosIncorrectos(), 'error'=> $validator->messages()]);
                }
                if($request->hasFile('image')) {
                    $file = $request->file('image');
                    $name = time() . '_' . $file->getClientOriginalName();
                    $path = base_path() .'/public_html/documents/';
                   // $this->correctImageOrientation($path.$name);
                    $file->move($path, $name);

                    $client = new Client();

                    $response = $client->post("https://bijao.uninorte.edu.co/cargar_foto/recibir_foto.php",
                    [

                        'multipart' => [
                            [
                                'name'     => 'inputFoto',
                                'contents'      => fopen($path . $name, 'r'),
                            ],
                            [
                                'name' => 'documento',
                                'contents' => $request->code
                            ],
                            [
                                'name' => 'secretToken',
                                'contents' => $tokenApp['tokenapp'][0]
                            ]
                        ],

                    ]);
                    if($response->getStatusCode() == 200){
                        $body = $response->getBody();
                        $stringBody = (string) $body;
                        $image_path = $path . $name;  // Value is not URL but directory file path
                        if(File::exists($image_path)) {
                            File::delete($image_path);
                        }
                        return $this->toJson($this->estadoExitoso(),json_decode($stringBody));
                    }
                    return $this->toJson($this->estadoOperacionFallida());
                }
                return $this->toJson($this->estadoOperacionFallida('No se selecciono una foto'));
            }
            return $this->toJson($this->estadoNoAutorizado("Token de app no valido"));
        }
        return $this->toJson($this->estadoNoAutorizado());
    }


    /**
        * Ver estado foto
        * @authenticated
        * @group Administración de usuario
        * @bodyParam $code int cedula de usuario
        * @response
        * {
        *     "status": {
        *         "code": 1,
        *         "message": "Procesado con éxito"
        *     },
        *     "data": {
        *         "state": "Pendiente",
        *         "response": 200,
        *         "message": "Estado Consultado Correctamente"
        *     }
        * }
        * @response
        * {
        *     "status": {
        *         "code": 1,
        *         "message": "Procesado con éxito"
        *     },
        *     "data": {
        *         "state": "",
        *         "response": 200,
        *         "message": "No hay Fotos Registradas"
        *     }
        * }
    */
    public function photoStates(Request $request){
        $tokenApp = $request->headers->all();
        if(array_key_exists('tokenapp', $tokenApp)){
            $secretToken = config('app.secretToken');

            if($tokenApp['tokenapp'][0] == $secretToken ){

                $client = new Client();

                $response = $client->post("https://bijao.uninorte.edu.co/cargar_foto/estado_foto.php",
                         [
                            'form_params' => [
                                'documento' => $request->code,
                                'secretToken' => $tokenApp['tokenapp'][0]
                            ]
                ]);
                if($response->getStatusCode() == 200){
                    $body = $response->getBody();
                    $stringBody = (string) $body;
                    return $this->toJson($this->estadoExitoso(),json_decode($stringBody));
                }
                return $this->toJson($this->estadoOperacionFallida());
            }
            return $this->toJson($this->estadoNoAutorizado("Token de app no valido"));
        }
        return $this->toJson($this->estadoNoAutorizado());
    }

    /**
	 * Enviar petición de tarjeta
     *
     * @authenticated
     * @group Administración de usuario
     * @bodyParam $message string mensaje que envia el usuario
     * @response
     *
     * {
     *   "status": {
     *       "code": 1,
     *       "message": "Procesado con éxito"
     *       }
     *   }
     *
     * @response
     * {
     *     "status": {
     *         "code": 401,
     *         "message": "Token de app no valido"
     *     }
     * }
     *
     * @response
     * {
     *     "status": {
     *         "code": 401,
     *         "message": "No autorizado"
     *     }
     * }
	*/
    public function sendRequestCard(Request $request){
        $tokenApp = $request->headers->all();
        if(array_key_exists('tokenapp', $tokenApp)){
            $secretToken = config('app.secretToken');
            if($tokenApp['tokenapp'][0] == $secretToken ){

                $rules = [
                    'message'       => 'required'
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails()) {
                    return response()->json(['status'=> $this->estadoParametrosIncorrectos(), 'error'=> $validator->messages()]);
                }
                $email = 'carnetizacion@uninorte.edu.co';
                //$email = 'martind@uninorte.edu.co';
                $subject = 'Carnetización';
                $logo = public_path().'/images/logo.jpeg';
                $bodyMessage =  $request->message;
                $correoEletronico = explode("<br>",$bodyMessage);
                $correo = explode(':',$correoEletronico[4])[1];  
                $correo = trim($correo);
                Mail::send(
                    ['html' => 'email.sendrequest'],
                    ['bodyMessage' => $bodyMessage, 'logo' => $logo],
                    function ($mail) use ($email, $subject,$correo) {
                        $mail->from(env('FROM_EMAIL_ADDRESS'), "Uninorte");
                        $mail->to($email)->cc($correo);
                        //$mail->to($email)->cc('medm89@gmail.com');
                        $mail->subject($subject);
                    }
                );
                return $this->toJson($this->estadoExitoso());
            }
            return $this->toJson($this->estadoNoAutorizado("Token de app no valido"));
        }
        return $this->toJson($this->estadoNoAutorizado());
    }


    public function sendRequestCardTest(Request $request){
        $tokenApp = $request->headers->all();
        if(array_key_exists('tokenapp', $tokenApp)){
            $secretToken = config('app.secretToken');
            if($tokenApp['tokenapp'][0] == $secretToken ){

                $rules = [
                    'message'       => 'required'
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails()) {
                    return response()->json(['status'=> $this->estadoParametrosIncorrectos(), 'error'=> $validator->messages()]);
                }
                $email = 'carnetizacion@uninorte.edu.co';
                //$email = 'pimentelj@uninorte.edu.co';
                $subject = 'Carnetización';
                $logo = public_path().'/images/logo.jpeg';
                $bodyMessage =  $request->message;
                $correoEletronico = explode("\n",$bodyMessage);
                $correo = explode(':',$correoEletronico[4])[1];  
                $correo = trim($correo);
                // Mail::send(
                //     ['html' => 'email.sendrequest'],
                //     ['bodyMessage' => $bodyMessage, 'logo' => $logo],
                //     function ($mail) use ($email, $subject) {
                //         $mail->from(env('FROM_EMAIL_ADDRESS'), "Uninorte");
                //         $mail->to($email)->cc('csu@uninorte.edu.co');
                //         //$mail->to($email)->cc('medm89@gmail.com');
                //         $mail->subject($subject);
                //     }
                // );
                dd($correo);
                return $this->toJson($this->estadoExitoso());
            }
            return $this->toJson($this->estadoNoAutorizado("Token de app no valido"));
        }
        return $this->toJson($this->estadoNoAutorizado());
    }
    /**
        * Cambiar contraseña de usuario
        * @authenticated
        * @group Administración de usuario
        * @bodyParam $email int email del usuario
        * @bodyParam $password string Nueva contraseña
        * @response
        * {
        *     "status": {
        *         "code": 1,
        *         "message": "Procesado con éxito"
        *     }
        * }
        *
        * @response
        * {
        *     "status": {
        *         "code": -1,
        *         "message": "No se encuentra el email"
        *     }
        * }
        *
        * @response
        * {
        *     "status": {
        *         "code": 401,
        *         "message": "Token de app no valido"
        *     }
        * }
        *
        * @response
        * {
        *     "status": {
        *         "code": 401,
        *         "message": "No autorizado"
        *     }
        * }
    */
    public function changePassword(Request $request){
        $tokenApp = $request->headers->all();
        if(array_key_exists('tokenapp', $tokenApp)){
            $secretToken = config('app.secretToken');
            if($tokenApp['tokenapp'][0] == $secretToken ){
                $rules = [
                    'email'       => 'required:max:255',
                    'password'    => 'required:max:255'
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails()) {
                    return response()->json(['status'=> $this->estadoParametrosIncorrectos(), 'error'=> $validator->messages()]);
                }
                $email = User::where('email',$request->email)->first();
                if($email){
                    $email->password = Hash::make($request->password);
                    $email->save();
                    return $this->toJson($this->estadoExitoso());
                }
                return $this->toJson($this->estadoOperacionFallida('No se encuentra el email'));

            }
            return $this->toJson($this->estadoNoAutorizado("Token de app no valido"));
        }
        return $this->toJson($this->estadoNoAutorizado());
    }

    //FUNCION PARA OBTENER UN DISPOSITVO DE USUARIO LOGEADO
    function getDevice($email){
        $device = userPhoneModel::where('user_code',$email)->first();
        return $this->toJson($this->estadoExitoso(),$device);
    }


    public function getAllAdmin(Request $request){
        $tokenApp = $request->headers->all();
        if(array_key_exists('tokenapp', $tokenApp)){
            $secretToken = config('app.secretToken');
            if($tokenApp['tokenapp'][0] == $secretToken ){
                $user = adminModel::where('status','A')->get();  //::where('code', $request->code)->first();
                return response()->json(['status'=> $this->estadoExitoso(), 'data'=>  $user ]);
            }
            return $this->toJson($this->estadoNoAutorizado("Token de app no valido"));
        }
        return $this->toJson($this->estadoNoAutorizado());
    }

    public function delAdmin(Request $request){
        $tokenApp = $request->headers->all();
        if(array_key_exists('tokenapp', $tokenApp)){
            $secretToken = config('app.secretToken');
            if($tokenApp['tokenapp'][0] == $secretToken ){
                $user = adminModel::where('id',$request->id)->delete();  //::where('code', $request->code)->first();
                return response()->json(['status'=> $this->estadoExitoso(), 'data'=>  $user ]);
            }
            return $this->toJson($this->estadoNoAutorizado("Token de app no valido"));
        }
        return $this->toJson($this->estadoNoAutorizado());
    }
    // ACCESOS DE USUARIO POR APP Y PORTAL
    public function getUserAccess(Request $request){
        $tokenApp = $request->headers->all();
        if(array_key_exists('tokenapp', $tokenApp)){
            $secretToken = config('app.secretToken');
            if($tokenApp['tokenapp'][0] == $secretToken ){
                $rules = [
                    'code'        => 'max:255',
                    'email'       => 'max:255',
                    'dateInit'    => 'date|nullable',
                    'dateEnd'     => 'date|after_or_equal:dateInit|nullable'

                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails()) {
                    return response()->json(['status'=> $this->estadoParametrosIncorrectos(), 'error'=> $validator->messages()]);
                }else{
                    $mes =3;
                    if($request->mes){$mes =$request->mes;}
                    if($request->dateInit){$dateInit = Carbon::createFromFormat('Y-m-d H:i:s',$request->dateInit." 00:00:00");}else{ $dateInit = Carbon::now()->subMonths($mes);}
                    if($request->dateEnd){$dateEnd = Carbon::createFromFormat('Y-m-d H:i:s',$request->dateEnd." 23:59:59");}else{ $dateEnd = Carbon::now();}
                }
                switch ($request->option) {
                    case 1:
                        // pide los logs ingreso Portal o la App
                        if($request->code == ''){
                            $logs = logs::where('message','like','%'.$request->email.'%')
                        ->where('systemMessage','!=',2)
                        ->whereBetween('created_at',[$dateInit, $dateEnd])
                        ->orderBy('created_at', 'DESC')
                        ->get();
                        }else{
                            $logs = logs::where('code',$request->code)
                        ->where('message','like','%'.$request->email.'%')
                        ->where('systemMessage','!=',2)
                        ->whereBetween('created_at',[$dateInit, $dateEnd])
                        ->orderBy('created_at', 'DESC')
                        ->get();
                        }
                        
                    break;

                    case 2:
                        // pide las desvinculaciones
                        if($request->code == ''){
                            $logs = logs::where('message','like','%'.$request->email.'%')
                        ->where('systemMessage',2)
                        ->whereBetween('created_at',[$dateInit, $dateEnd])
                        ->orderBy('created_at', 'DESC')
                        ->get();
                        }else{
                            $logs = logs::where('code',$request->code)
                        ->where('message','like','%'.$request->email.'%')
                        ->where('systemMessage',2)
                        ->whereBetween('created_at',[$dateInit, $dateEnd])
                        ->orderBy('created_at', 'DESC')
                        ->get();
                        }

                     break;

                    default:
                    $mes =3;
                    if($request->mes){$mes =$request->mes;}
                    if($request->dateInit){$dateInit = Carbon::createFromFormat('Y-m-d H:i:s',$request->dateInit." 00:00:00");}else{ $dateInit = Carbon::now()->subMonths($mes);}
                    if($request->dateEnd){$dateEnd = Carbon::createFromFormat('Y-m-d H:i:s',$request->dateEnd." 23:59:59");}else{ $dateEnd = Carbon::now();}
                    // $logs = logs::where('systemMessage','!=',2)
                    $logs = logs::whereBetween('created_at',[$dateInit, $dateEnd])
                    ->orderBy('created_at', 'DESC')
                    ->get();
                        break;
                }
              return $this->toJson($this->estadoExitoso(),$logs);
            }
            return $this->toJson($this->estadoNoAutorizado("Token de app no valido"));
        }
        return $this->toJson($this->estadoNoAutorizado());
    }

    public function getReportUsers(Request $request){
        $tokenApp = $request->headers->all();
        if(array_key_exists('tokenapp', $tokenApp)){
            $secretToken = config('app.secretToken');
            if($tokenApp['tokenapp'][0] == $secretToken ){
                $rules = [
                    'code'        => 'max:255|nullable',
                    'option'       => 'max:255',
                    'dateInit'    => 'date|nullable',
                    'dateEnd'     => 'date|after_or_equal:dateInit|nullable'

                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails()) {
                    return response()->json(['status'=> $this->estadoParametrosIncorrectos(), 'error'=> $validator->messages()]);
                }else{
                    
                    if($request->dateInit){$dateInit = Carbon::createFromFormat('Y-m-d H:i:s',$request->dateInit." 00:00:00");}else{ $dateInit = Carbon::now()->subMonths(100);}
                    if($request->dateEnd){$dateEnd = Carbon::createFromFormat('Y-m-d H:i:s',$request->dateEnd." 23:59:59");}else{ $dateEnd = Carbon::now();}
                }
                
                switch ($request->option) {
                    case 1:
                        // Dispositivos x usuarios
                        $user = userPhoneModel::select('user_code', 'user_email_un', 'user_email_ext', 'user_phone_model', 'user_phone_brand', 'user_phone_movilso', 'user_phone_versionso','created_at')
                        ->whereBetween('created_at',[$dateInit, $dateEnd])
                        ->orderBy('created_at', 'DESC')
                        ->get();
                        $consulta = userPhoneModel::select('user_code', 'user_email_un', 'user_email_ext', 'user_phone_model', 'user_phone_brand', 'user_phone_movilso', 'user_phone_versionso','created_at')
                        ->whereBetween('created_at',[$dateInit, $dateEnd])
                        ->orderBy('created_at', 'DESC')
                        ->toSql();
                        // dd($consulta);
                    break;
                    case 2:
                        // Dispositivo Actual de un usuario se envia la cedula.
                         if($request->code==''){
                            $user = userPhoneModel::select('user_code', 'user_email_un', 'user_email_ext', 'user_phone_model', 'user_phone_brand', 'user_phone_movilso', 'user_phone_versionso','created_at')
                            ->whereBetween('created_at',[$dateInit, $dateEnd])
                            ->where('user_email_un','like','%'.$request->email.'%')
                            ->orwhere('user_email_ext','like','%'.$request->email.'%')
                            ->get();

                            $consulta = userPhoneModel::select('user_code', 'user_email_un', 'user_email_ext', 'user_phone_model', 'user_phone_brand', 'user_phone_movilso', 'user_phone_versionso','created_at')
                            ->whereBetween('created_at',[$dateInit, $dateEnd])
                            ->where('user_email_un','like','%'.$request->email.'%')
                            ->orwhere('user_email_ext','like','%'.$request->email.'%')
                            ->toSql();
                            // dd($consulta);
                            
                         }else{
                            $user = userPhoneModel::select('user_code', 'user_email_un', 'user_email_ext', 'user_phone_model', 'user_phone_brand', 'user_phone_movilso', 'user_phone_versionso','created_at')
                            ->whereBetween('created_at',[$dateInit, $dateEnd])
                            ->where('user_code',$request->code)
                            ->get();

                            $consulta = userPhoneModel::select('user_code', 'user_email_un', 'user_email_ext', 'user_phone_model', 'user_phone_brand', 'user_phone_movilso', 'user_phone_versionso','created_at')
                            ->whereBetween('created_at',[$dateInit, $dateEnd])
                            ->where('user_code',$request->code)
                            ->toSql();
                            // dd($consulta);
                         }
                         
                    break;
                    case 3:
                        //Accesos del usuario
                        if($request->email == ''){
                         $users = logs::where('code',$request->code)
                         ->whereBetween('created_at',[$dateInit, $dateEnd])
                         ->orderBy('created_at', 'DESC')
                        ->get();
                        }else{
                          $users = logs::where('message','like','%'.$request->email.'%')
                          ->whereBetween('created_at',[$dateInit, $dateEnd])
                          ->orderBy('created_at', 'DESC')
                          ->get(); 
                        }
                        
                        foreach($users as $reg)
                        {
                          if($reg->systemMessage == 6){
                            $raspberry = raspberry::where('code',$reg->message)->first();
                            if($raspberry){
                                $reg->message = $raspberry->name; 
                            }
                          }
                        }
                        $user = $users;
                    break;
                    default:

                    break;
                }
                return response()->json(['status'=> $this->estadoExitoso(), 'data'=>  $user ]);
            }
            return $this->toJson($this->estadoNoAutorizado("Token de app no valido"));
        }
        return $this->toJson($this->estadoNoAutorizado());
    }

    /** PARA IMPLEMENTAR EN UN FUTURO */
    public function getAllUser(Request $request){
        $columns = array(
            0 =>'id',
            1 =>'name',
            2=> 'email'
        );

        $totalData = User::count();
        $totalFiltered = $totalData;
        $start  = $request->input('start');
        $limit  = $request->input('length');
        $search = $request['search']['value'];
        $order = $columns[$request->input('order.0.column')];
        $data = array();

        if( $search == null){
            $users = User::select('id','code','email')
            ->skip($start)
            ->take($limit)
            ->get();
            $totalData = User::select('id','code','email')->count();
            $totalFiltered = $totalData;
        }else{
            $users = User::select('id','code','email')->where(function ($query) use ($search) {
                    $query->where('id','like',"%{$search}%")
                    ->orWhere('name','like',"%{$search}%")
                    ->orWhere('email','like',"%{$search}%");
                })
                ->orderBy($order)
                ->get();
                $totalData = User::select('id','code','email')->count();
                $totalFiltered = $totalData;
        }

        $usuarios = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $users
        );
        return $usuarios;
    }



    public function validateUserAccess(Request $request){
        $tokenApp = $request->headers->all();
        if(array_key_exists('tokenapp', $tokenApp)){
            $secretToken = config('app.secretToken');
            if($tokenApp['tokenapp'][0] == $secretToken ){

                $rules = [
                    'id_usuario'       => 'required',
                    'id_dispositivo'   => 'required',
                    'timestands'   => 'required'
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails()) {
                    return response()->json(['status'=> $this->estadoParametrosIncorrectos(), 'error'=> $validator->messages()]);
                }
                $user = userAccess::select('ua_usuario', 'ua_dispositivo','ua_timestands','created_at')
                        ->where('ua_usuario',$request->id_usuario)
                        ->Where('ua_dispositivo',$request->id_dispositivo)
                        ->Where('ua_timestands',$request->timestands)
                        ->orderBy('created_at', 'DESC')
                        ->first();
                if(!$user){
                    userAccess::create(['ua_usuario'=>$request->id_usuario,'ua_dispositivo'=>$request->id_dispositivo,'ua_timestands'=>$request->timestands]);
                    $respuesta = 1;
                }else{
                    if( $request->timestands == $user->ua_timestands ){
                        $respuesta = 2;
                    }else{
                        $respuesta = 1;
                    }
                }
                return response()->json(['status'=> $this->estadoExitoso(), 'acceso'=>$respuesta ,'data'=>  $user ]);
            }
            return $this->toJson($this->estadoNoAutorizado("Token de app no valido"));
        }
        return $this->toJson($this->estadoNoAutorizado());
    }

    // FUNCION PARA ACTUALIZAR CORREO DE UN USUARIO EXTERNO
    public function changeEmail(Request $request){
        $tokenApp = $request->headers->all();
        if(array_key_exists('tokenapp', $tokenApp)){
            $secretToken = config('app.secretToken');
            if($tokenApp['tokenapp'][0] == $secretToken ){
                $rules = [
                    'cedula'       => 'required:max:255',
                    'email'    => 'required:max:255'
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails()) {
                    return response()->json(['status'=> $this->estadoParametrosIncorrectos(), 'error'=> $validator->messages()]);
                }
                $email = User::where('code',$request->cedula)->first();
                if($email){
                    $email->email = $request->email;
                    $pass = substr( md5(microtime()), 1, 10);
                    $email->password=Hash::make($pass);
                    $userPhone = userPhoneModel::where('user_code',$request->cedula)->first();
                    if($userPhone){
                     $userPhone->user_email_ext = $request->email;
                     $userPhone->save();                     
                    }
                    $email->save();
                    $bodyMessage = "Estimado usuario,<br>
                    Su correo ha sido actualizado y a su vez su usuario de ingreso a la app ID Uninorte.<br>
                    Su usuario es <strong>".$request->email."</strong> y su contraseña <strong>".$pass."</strong>.<br>
                    Esta contraseña es temporal y debe cambiarla una vez ingrese a la app.<br>
                    Si usted ya es un usuario activo, debe cambiar nuevamente su contraseña.<br>
                    <br>
                    Para cualquier inquietud puede escribir a carnetizacion@uninorte.edu.co";
                    $subject = "Registro ID Uninorte";
                    $email =trim($request->email);
                    $logo = public_path().'/images/logo.jpeg';
                    Mail::send(
                            ['html' => 'email.sendrequest'],
                            ['bodyMessage' => $bodyMessage, 'logo' => $logo],
                            function ($mail) use ($email, $subject) {
                                $mail->from(env('FROM_EMAIL_ADDRESS'), "Uninorte");
                                $mail->to($email)->cc('delaossak@uninorte.edu.co');
                                $mail->subject($subject);
                            }
                        );
                    
                    return $this->toJson($this->estadoExitoso());
                }
                return $this->toJson($this->estadoOperacionFallida('No se encuentra el email'));

            }
            return $this->toJson($this->estadoNoAutorizado("Token de app no valido"));
        }
        return $this->toJson($this->estadoNoAutorizado());
    }

    // FUNCION PARA ACTUALIZAR CORREO DE UN USUARIO EXTERNO
    public function createUserExt(Request $request){
        $tokenApp = $request->headers->all();
        if(array_key_exists('tokenapp', $tokenApp)){
            $secretToken = config('app.secretToken');
            if($tokenApp['tokenapp'][0] == $secretToken ){
                $rules = [
                    'email'       => 'required:max:255',
                    'cedula'       => 'required:max:255'
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails()) {
                    return response()->json(['status'=> $this->estadoParametrosIncorrectos(), 'error'=> $validator->messages()]);
                }
                $email = User::where('code',$request->cedula)->first();
                if(!$email){
                    $pass = substr( md5(microtime()), 1, 10);
                    $userCreate = User::create(['code'=>trim($request->cedula), 'email'=>trim($request->email), 'password'=>Hash::make($pass)]);
                    $bodyMessage = "Estimado usuario,<br>
                    A partir de este momento puede hacer uso de la app ID Uninorte para generar su carné.<br>
                    Su usuario es <strong>".$request->email."</strong> y su contraseña <strong>".$pass."</strong>.<br>
                    Esta contraseña es temporal y debe cambiarla una vez ingrese a la app.<br>
                    Si usted ya es un usuario activo, debe cambiar nuevamente su contraseña.<br>
                    <br>
                    Para cualquier inquietud puede escribir a carnetizacion@uninorte.edu.co";
                    $subject = "Registro ID Uninorte";
                    $email =trim($request->email);
                    $logo = public_path().'/images/logo.jpeg';
                    
                    if($userCreate){        
                        Mail::send(
                            ['html' => 'email.sendrequest'],
                            ['bodyMessage' => $bodyMessage, 'logo' => $logo],
                            function ($mail) use ($email, $subject) {
                                $mail->from(env('FROM_EMAIL_ADDRESS'), "Uninorte");
                                $mail->to($email);
                                //$mail->to($email)->cc('medm89@gmail.com');
                                $mail->subject($subject);
                            }
                        );
                        return $this->toJson($this->estadoExitoso());
                    }else{
                        return $this->toJson($this->estadoOperacionFallida('No se pudo crear el usuario'));
                    }
                }
                return $this->toJson($this->estadoOperacionFallida('No se encuentra el email'));

            }
            return $this->toJson($this->estadoNoAutorizado("Token de app no valido"));
        }
        return $this->toJson($this->estadoNoAutorizado());
    }

       /**
        * Obtener dispositivos del usuario
        * @authenticated
        * @group Dispositivo
        * @bodyParam $email string email del usuario
        * @response
        * {
        *        "status": {
        *            "code": 1,
        *            "message": "Procesado con éxito"
        *        },
        *        "data": {
        *            "user_id": 3,
        *            "user_email_un": null,
        *            "user_email_ext": "jahel131296@gmail.com",
        *            "user_uid": "456789",
        *            "user_phone_model": null,
        *            "user_phone_brand": null,
        *            "user_last_access": null,
        *            "updated_at": "2020-03-05 22:34:39.023",
        *            "created_at": "2020-05-03 16:58:53.393"
        *        }
        *    }
        *
	*/
    public function activateCovidTest(Request $request){
        $tokenApp = $request->headers->all();
        if(array_key_exists('tokenapp', $tokenApp)){
            $secretToken = config('app.secretToken');
            if($tokenApp['tokenapp'][0] == $secretToken ){

                $rules = [
                    'option'       => 'required:max:255'
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails()) {
                    return response()->json(['status'=> $this->estadoParametrosIncorrectos(), 'error'=> $validator->messages()]);
                }
                $options = accessCovid::where('id',1)->first();
                if($options){
                    $options->options = $request->option;
                    $options->save();
                }else{
                    accessCovid::create(['options'=>$request->option]);
                }
                return $this->toJson($this->estadoExitoso(),$options);
            }
            return $this->toJson($this->estadoNoAutorizado("Token de app no valido"));
        }
        return $this->toJson($this->estadoNoAutorizado());
    }

    public function getCovidTest(Request $request){
        $tokenApp = $request->headers->all();
        if(array_key_exists('tokenapp', $tokenApp)){
            $secretToken = config('app.secretToken');
            if($tokenApp['tokenapp'][0] == $secretToken ){
                $options = accessCovid::where('id',1)->first();
                return $this->toJson($this->estadoExitoso(),$options);
            }
            return $this->toJson($this->estadoNoAutorizado("Token de app no valido"));
        }
        return $this->toJson($this->estadoNoAutorizado());
    }
    
    public function delInfoUser(Request $request){
            $tokenApp = $request->headers->all();
            if(array_key_exists('tokenapp', $tokenApp)){
                $secretToken = config('app.secretToken');
                if($tokenApp['tokenapp'][0] == $secretToken ){
                    
                    $user = User::where('code',$request->code)->delete(); 
                    $user = userPhoneModel::where('user_code',$request->code)->delete();
                    return response()->json(['status'=> $this->estadoExitoso(), 'data'=>  $user ]);
                }
                return $this->toJson($this->estadoNoAutorizado("Token de app no valido"));
            }
            return $this->toJson($this->estadoNoAutorizado());
        }
    protected function objectToJson($result){
        $content = $result->getContent();
        $array = json_decode($content, true);
        return $array;
    }
}
