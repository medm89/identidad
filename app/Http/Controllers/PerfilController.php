<?php

namespace App\Http\Controllers;

use JWT;
use App\Models\perfil;
use Illuminate\Http\Request;
use App\Classes\FormatResponse;
use Input;
use Validator, DB, Hash, Mail; 

class PerfilController extends FormatResponse
{
    public function  __construct(){
        $this->middleware('cors'); 
        $this->middleware('jwt');
    }
    
    /**
	   * Obtener todas las PERFILES
     * @authenticated
     * @group Preguntas Frecuentes
     * 
	  */ 
    public function getPerfilInterno ($perfiles){
        $perfilArr = explode("-",$perfiles);
        $arr = array();
        if(count($perfilArr) > 0){
            for ($i=0; $i < count($perfilArr) ; $i++) { 
                $color = perfil::where('name',$perfilArr[$i])->first();
                $temp = array('name' => $color->name, 'color' => $color->color, 'titulo' => $color->carnet);
                array_push($arr,$temp);
            }
        }
          return json_encode($arr);
        
   }

   public function getPerfil (Request $request){
    $secretToken = config('app.secretToken');
    if($request->tokenApp = $secretToken){
    
            $perfil = perfil::orderBy('created_at', 'DESC')->get();
            return $this->toJson($this->estadoExitoso(),$perfil);
            
      }
      
    
    return $this->toJson($this->estadoNoAutorizado("Token de app no valido"));  
}

   /**
	   * Obtener todas las PERFILES
     * @authenticated
     * @group Preguntas Frecuentes
     * 
	  */ 
    public function createPerfil (Request $request){
      $secretToken = config('app.secretToken');
        if($request->tokenApp = $secretToken){
        try{ 
            $rules = [
                'name'    => 'required',
                'color'  => 'required',
                'titulo'  => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if($validator->fails()) {
                return response()->json(['status'=> $this->estadoParametrosIncorrectos(), 'error'=> $validator->messages()]);
            }
            $perfil = perfil::create(['name'=>$request->name,'color'=>$request->color,'carnet'=>$request->titulo]);
            return $this->toJson(['status'=> $this->estadoExitoso(), 'data'=>  $perfil ]);

        }catch (JWTException $e){
            return $this->toJson($this->estadoOperacionFallida($e));        
        }
     }else{
        return $this->toJson($this->estadoNoAutorizado()); 
     }
    }
    public function delPerfil(Request $request){
      $secretToken = config('app.secretToken');
        if($request->tokenApp = $secretToken){
            $perfil = perfil::where('id',$request->id)->delete();  //::where('code', $request->code)->first();
             return response()->json(['status'=> $this->estadoExitoso(), 'data'=>  $perfil ]);
        }
      return $this->toJson($this->estadoNoAutorizado("Token de app no valido"));
    }
    public function updatePerfil (Request $request){
        $secretToken = config('app.secretToken');
          if($request->tokenApp = $secretToken){
          try{ 
              $rules = [
                  'titulo'    => 'required',
                  'name'    => 'required',
                  'color'  => 'required',
                  'id'       => 'required',
              ];
              $validator = Validator::make($request->all(), $rules);
              if($validator->fails()) {
                  return response()->json(['status'=> $this->estadoParametrosIncorrectos(), 'error'=> $validator->messages()]);
              }
              $perfil = perfil::where('id',$request->id)->first();
              if($perfil){
                $perfil->carnet   =  $request->titulo;
                $perfil->name   =  $request->name;
                $perfil->color =  $request->color;
              }
              $perfil->save();
              return $this->toJson($this->estadoExitoso());
  
          }catch (JWTException $e){
              return $this->toJson($this->estadoOperacionFallida($e));        
          }
       }else{
          return $this->toJson($this->estadoNoAutorizado()); 
       }
      }

}