<?php

namespace App\Http\Controllers;

use JWT;
use App\Models\faqs;
use Illuminate\Http\Request;
use App\Classes\FormatResponse;
use Input;
use Validator, DB, Hash, Mail; 

class FaqsController extends FormatResponse
{
    public function  __construct(){
        $this->middleware('cors'); 
        $this->middleware('jwt');
    }
    
    /**
	   * Obtener todas las FAQS
     * @authenticated
     * @group Preguntas Frecuentes
     * 
	  */ 
    public function getFaqs (Request $request){
        $secretToken = config('app.secretToken');
        if($request->tokenApp = $secretToken){
          
          switch ($request->option) {
            case 1:
                // pide las faqs solo para movil solamente
                $faqs = faqs::where('device',1)
                ->orWhere('device',3)
                ->orderBy('created_at', 'DESC')
                ->get();
            break;
            case 2:
                // pide las faqs solo para web solamente
                $faqs = faqs::where('device',2)
                ->orWhere('device',3)
                ->orderBy('created_at', 'DESC')
                ->get();
             break;
            default:
                // pide las faqs solo para desktop y movil
                $faqs = faqs::orderBy('created_at', 'DESC')
                ->get();
                break;
          }
          return $this->toJson($this->estadoExitoso(),$faqs);
        }
        return $this->toJson($this->estadoNoAutorizado("Token de app no valido"));  
   }

   /**
	   * Obtener todas las FAQS
     * @authenticated
     * @group Preguntas Frecuentes
     * 
	  */ 
    public function createFaqs (Request $request){
      $secretToken = config('app.secretToken');
        if($request->tokenApp = $secretToken){
        try{ 
            $rules = [
                'title'    => 'required',
                'content'  => 'required',
                'device'   => 'required'
            ];
            $validator = Validator::make($request->all(), $rules);
            if($validator->fails()) {
                return response()->json(['status'=> $this->estadoParametrosIncorrectos(), 'error'=> $validator->messages()]);
            }
            $faqs = faqs::create(['title'=>$request->title,'content'=>$request->content,'device'=>$request->device]);
            return $this->toJson(['status'=> $this->estadoExitoso(), 'data'=>  $faqs ]);

        }catch (JWTException $e){
            return $this->toJson($this->estadoOperacionFallida($e));        
        }
     }else{
        return $this->toJson($this->estadoNoAutorizado()); 
     }
    }
    public function delFaqs(Request $request){
      $secretToken = config('app.secretToken');
        if($request->tokenApp = $secretToken){
            $faqs = faqs::where('id',$request->id)->delete();  //::where('code', $request->code)->first();
             return response()->json(['status'=> $this->estadoExitoso(), 'data'=>  $faqs ]);
        }
      return $this->toJson($this->estadoNoAutorizado("Token de app no valido"));
    }
    public function updateFaqs (Request $request){
        $secretToken = config('app.secretToken');
          if($request->tokenApp = $secretToken){
          try{ 
              $rules = [
                  'title'    => 'required',
                  'content'  => 'required',
                  'device'   => 'required',
                  'id'       => 'required',
              ];
              $validator = Validator::make($request->all(), $rules);
              if($validator->fails()) {
                  return response()->json(['status'=> $this->estadoParametrosIncorrectos(), 'error'=> $validator->messages()]);
              }
              $faqs = faqs::where('id',$request->id)->first();
              if($faqs){
                $faqs->title   =  $request->title;
                $faqs->content =  $request->content;
                $faqs->device  =  $request->device;
              }
              $faqs->save();
              return $this->toJson($this->estadoExitoso());
  
          }catch (JWTException $e){
              return $this->toJson($this->estadoOperacionFallida($e));        
          }
       }else{
          return $this->toJson($this->estadoNoAutorizado()); 
       }
      }

}