<?php

namespace App\Http\Controllers;

use Auth;
use JWTAuth; 
use Illuminate\Http\Request;
use App\Classes\FormatResponse;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException; 

class AuthController extends FormatResponse
{
    public function __construct()
    {
        $this->middleware('jwt');
    }
     /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);
        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->respondWithToken($token);
    }
    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }
    public function payload()
    {
        return response()->json(auth()->payload());
    }
    /**
     * Cerrar sesión
     * @authenticated
     * 
     * @group Administración de usuario
     * @response 
     * {
     *       "status": {
     *           "code": 1,
     *           "message": "Procesado con éxito"
     *       }
     *   }
     * 
    */
    public function logout()
    {
        auth()->logout();
        return $this->toJson($this->estadoExitoso()); 
    }
    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }
    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user(),
        ]);
    }
}
