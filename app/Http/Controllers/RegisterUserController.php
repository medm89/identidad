<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\FormatResponse;
use App\Models\userPhoneModel;
use App\Models\adminModel;
use Validator, DB, Hash, Mail;
use JWT;

class RegisterUserController extends FormatResponse
{
    public function  __construct(){
        $this->middleware('cors');
        $this->middleware('jwt');
    }

    /**
	 * Registro de dispositivo
     * @group Administración de usuario
     * @bodyParam $user_uid string identificador del dispositivo
     * @bodyParam $user_email_un string email del usuario  si es de la norte
     * @bodyParam $user_email_ext string email del usuario  si es externo
     * @bodyParam $user_code int cedula del usuario
     * @bodyParam $user_phone_model string modelo del celular
     * @bodyParam $user_phone_brand string marca del celular
    */
    public function registerDevice(Request $request)
    {
        try{
            $rules = [
                'user_uid'            => 'required|max:255',
                'user_email_un'       => 'max:255',
                'user_email_ext'      => 'max:255',
                'user_code'           => 'max:255',
                'user_phone_model'    => 'required',
                'user_phone_brand'    => 'required'
            ];
            $validator = Validator::make($request->all(), $rules);
            if($validator->fails()) {
                return response()->json(['status'=> $this->estadoParametrosIncorrectos(), 'error'=> $validator->messages()]);
            }else{
               $userD = userPhoneModel::where('user_uid',$request->user_uid)->count();
                 if($userD != 0){
                    return response()->json(['status'=> $this->estadoOperacionFallida("Este dispositivo ya fue registrado")]);
                 }else{
                    $userPhone = userPhoneModel::create($request->all());
                    if($userPhone){
                        return response()->json(['status'=> $this->estadoExitoso(), 'data'=>  $userPhone ]);
                    }else{
                        return response()->json(['status'=> $this->estadoOperacionFallida()]);
                    }
                 }
            }
        }catch (JWTException $e){
            return $this->toJson($this->estadoOperacionFallida($e));
        }
    }

    /**
        * Asignar permiso a administrador
        * @authenticated
        * @group Administración de usuario
        * @bodyParam $code int cedula de usuario
        * @bodyParam $is_admin int 1: es administrador, 0: no es administrador.
        * @response
        * {
        *     "status": {
        *         "code": 1,
        *         "message": "Procesado con éxito"
        *     }
        * }
        *
        * @response
        * {
        *     "status": {
        *         "code": -1,
        *         "message": "No se encuentra al usuario"
        *     }
        * }
        *
        * @response
        * {
        *     "status": {
        *         "code": 401,
        *         "message": "Token de app no valido"
        *     }
        * }
        *
        * @response
        * {
        *     "status": {
        *         "code": 401,
        *         "message": "No autorizado"
        *     }
        * }
    */
    public function registerAdmUser(Request $request){
        $secretToken = config('app.secretToken');
        if($request->tokenApp = $secretToken){
        try{
            $rules = [
                'email'     => 'required|max:255'
            ];
            $validator = Validator::make($request->all(), $rules);
            if($validator->fails()) {
                return response()->json(['status'=> $this->estadoParametrosIncorrectos(), 'error'=> $validator->messages()]);
            }
            if(explode("@",$request->email)[1] == "uninorte.edu.co"){
                $userValidation = adminModel::where('email', $request->email)->first();
                if($userValidation){
                    return response()->json(['status'=> $this->estadoOperacionFallida("El usuario ya existe"), 'data'=>  $userValidation]);
                }else{
                    $user = adminModel::create(['email' => $request->email,'status' => 'A']);
                if(!$user){
                      return response()->json(['status'=> $this->estadoOperacionFallida()]);
                }
                $userValidation = adminModel::where('email', $request->email)->first();
                return response()->json(['status'=> $this->estadoExitoso(), 'data'=>  $userValidation ]);
                 // enviar un correo
                }
             }else{
                return response()->json(['status'=> $this->estadoOperacionFallida("No se puede crear usuario externo como Administardor")]);
             }

        }catch (JWTException $e){
            return $this->toJson($this->estadoOperacionFallida($e));
        }
     }else{
        return $this->toJson($this->estadoNoAutorizado());
     }
    }
}
