<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\FormatResponse;
use Validator, DB, Hash, Mail;
use JWT;
use App\Models\userPhoneModel;
use Carbon\Carbon;
use App\Http\Controllers\PerfilController;
use Illuminate\Support\Facades\Log;

class PermissionCCureController extends FormatResponse
{
    public function  __construct(){
        $this->middleware('cors');
        $this->middleware('jwt');
    }

    public function getUserPermission(Request $request){
      Log::info('action Se solicita Datos de obtener usuario message'.$request->email);
      //  logs::create(['code'=>911,'action'=>'Se solicita Datos de obtener usuario ','message'=>$request->email,'systemMessage'=>35]);
      try {
        $user = userPhoneModel::where('user_email_un',$request->email)->orWhere('user_email_ext',$request->email)->first();
        if($user){
          Log::info('Se encontro usuario message'.$user->user_last_access);
          // logs::create(['code'=>911,'action'=>'Se encontro usuario','message'=>$user->user_last_access,'systemMessage'=>35]);
          $user->user_last_access = Carbon::now();
          $user->save();
        $jacarandaDB = \DB::connection('ccure');
        $jacaranda  = $jacarandaDB->select("exec access.getPermissionCcure ? ",array($request->code));
        if(!empty($jacaranda)){
          Log::info('Se encontro usuario usuario enviaod a jacaranda'.$request->code);
          foreach ($jacaranda as $result) {
            $perfiles = (new PerfilController)->getPerfilInterno($result->Perfiles);
            $perfiles = json_decode($perfiles);
            if(!empty($result->Thumbnail1)){
            $foto64 = base64_encode(pack('H*',$result->Thumbnail1).pack('H*',$result->Thumbnail2).pack('H*',$result->Thumbnail3).pack('H*',$result->Thumbnail4));
            $arr = array('id'=>$result->ID,'documento'=>$result->Documento,'nombre'=>$result->Nombre,'perfiles'=>$result->Perfiles,'perfilesnew'=>$perfiles,'foto'=>$foto64);
            $user->user_last_access = Carbon::now();
            $user->save();
           }else{
            $arr = array('id'=>$result->ID,'documento'=>$result->Documento,'nombre'=>$result->Nombre,'perfiles'=>$result->Perfiles,'perfilesnew'=>$perfiles,'foto'=>null);
            $user->user_last_access = Carbon::now();
            $user->save();
            return response()->json(['status'=> $this->estadoMedioExitoso("Permisos Obtenidos sin foto"), 'data'=> $arr ]);
           }
          }
          $user->user_last_access = Carbon::now();
          $user->save();
          return response()->json(['status'=> $this->estadoExitoso("Permisos Obtenidos"), 'data'=>  $arr ]);
        }else{
          return response()->json(['status'=> $this->estadoNoEncontrado("El usuario no tiene registros") ]);
        }
      }else{
        return response()->json(['status'=> $this->estadoNoEncontrado("Este dispositivo fue desvinculado de esta cuenta",2) ]);
      }
      } catch (Exception $e) {
        return $this->toJson($this->estadoOperacionFallida($e));
     }
    }

    public function getUserPermissionIn($code){
      try {
        $jacarandaDB = \DB::connection('ccure');
        $jacaranda  = $jacarandaDB->select("exec access.getPermissionCcure ? ",array($code));
        // $user = userPhoneModel::where('user_code',$code)->first();
        if(!empty($jacaranda)){
          foreach ($jacaranda as $result) {
            $perfiles = (new PerfilController)->getPerfilInterno($result->Perfiles);
            $perfiles = json_decode($perfiles);
            if(!empty($result->Thumbnail1)){
            $foto64 = base64_encode(pack('H*',$result->Thumbnail1).pack('H*',$result->Thumbnail2).pack('H*',$result->Thumbnail3).pack('H*',$result->Thumbnail4));
            $arr = array('id'=>$result->ID,'documento'=>$result->Documento,'nombre'=>$result->Nombre,'perfiles'=>$result->Perfiles,'perfilesnew'=>$perfiles,'foto'=>$foto64);
            // if($user){
            //   $user->user_last_access = Carbon::now();
            //   $user->save();              
            // }
           }else{
            $arr = array('id'=>$result->ID,'documento'=>$result->Documento,'nombre'=>$result->Nombre,'perfiles'=>$result->Perfiles,'perfilesnew'=>$perfiles,'foto'=>null);
            // if($user){
            //   $user->user_last_access = Carbon::now();
            //   $user->save();              
            // }
            return response()->json(['status'=> $this->estadoMedioExitoso("Permisos Obtenidos sin foto"), 'data'=> $arr ]);
           }
          }
          // if($user){
          //   $user->user_last_access = Carbon::now();
          //   $user->save();              
          // }
          return response()->json(['status'=> $this->estadoExitoso("Permisos Obtenidos"), 'data'=> $arr ]);
        }else{
          return response()->json(['status'=> $this->estadoNoEncontrado("El usuario no tiene registros") ]);
        }
      } catch (Exception $e) {
        return $this->toJson($this->estadoOperacionFallida($e));
     }
    }

    // permisos para el portal
    public function getUserPermissionPortal($code){
        try {
          $jacarandaDB = \DB::connection('ccure');
          $jacaranda  = $jacarandaDB->select("exec access.getPermissionCcure ? ",array($code));
          if(!empty($jacaranda)){
            foreach ($jacaranda as $result) {
              $perfiles = (new PerfilController)->getPerfilInterno($result->Perfiles);
              $perfiles = json_decode($perfiles);
              if(!empty($result->Thumbnail1)){
              //$foto64 = base64_encode(pack('H*',$result->Thumbnail1).pack('H*',$result->Thumbnail2).pack('H*',$result->Thumbnail3).pack('H*',$result->Thumbnail4));
              $arr = array('id'=>$result->ID,'documento'=>$result->Documento,'nombre'=>$result->Nombre,'perfiles'=>$result->Perfiles,'perfilesnew'=>$perfiles,'foto1'=>$result->Thumbnail1,'foto2'=>$result->Thumbnail2,'foto3'=>$result->Thumbnail3,'foto4'=>$result->Thumbnail4);
             }else{
              $arr = array('id'=>$result->ID,'documento'=>$result->Documento,'nombre'=>$result->Nombre,'perfiles'=>$result->Perfiles,'perfilesnew'=>$perfiles,'foto'=>null);
              return response()->json(['status'=> $this->estadoMedioExitoso("Permisos Obtenidos sin foto"), 'data'=> $arr ]);
             }
            }
            return response()->json(['status'=> $this->estadoExitoso("Permisos Obtenidos"), 'data'=> $arr ]);
          }else{
            return response()->json(['status'=> $this->estadoNoEncontrado("El usuario no tiene registros") ]);
          }
        } catch (Exception $e) {
          return $this->toJson($this->estadoOperacionFallida($e));
       }
      }

}
