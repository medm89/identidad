<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\FormatResponse;
use App\Models\userPhoneModel;
use App\User;
use Validator, DB, Hash, Mail;
use JWT;
use App\Models\logs; 
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\PayloadFactory;
use JWTFactory;

use App\Models\qr;
use App\Models\userAccessCovid;
use Illuminate\Contracts\Encryption\DecryptException;

class RegisterUserInController extends FormatResponse
{
    public function  __construct(){
        $this->middleware('cors');
    }

    public function getToken(Request $request){
      
        $factory = JWTFactory::customClaims([
            'sub'=> $array['data']["documento"],
        ]);
        $payload = $factory->make();
        $token = JWTAuth::encode($payload)->get();
        
    }
    /**
	 * Registro de Usuario externo
     * @group Administración de usuario
    */
    public function registerExtUserIN(Request $request){
        $secretToken = config('app.secretToken');
        $pass = '';
        if($request->tokenApp = $secretToken){
        try{ 
            $rules = [
                'email'     => 'required|max:255',
                'password'  => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if($validator->fails()) {
                return response()->json(['status'=> $this->estadoParametrosIncorrectos(), 'error'=> $validator->messages()]);
            }
            if(explode("@",$request->email)[1] != "uninorte.edu.co"){
                $userValidation = User::where('email', $request->email)->first();
                $documento = $request->password;
                if($userValidation){
                    $pass = substr( md5(microtime()), 1, 10);
                    $userValidation->password = Hash::make($pass);
                    $userValidation->save(); 
                    return response()->json(['status'=> $this->estadoOperacionFallida("El usuario ya existe"), 'data'=>  $userValidation, 'clave'=>$pass]);
                }else{
                    $documento = $request->password;
                    $pass = substr( md5(microtime()), 1, 10);
                    $user = User::create(['email' => $request->email,'password' => Hash::make($pass),'code' =>$documento]);
                if(!$user){
                      return response()->json(['status'=> $this->estadoOperacionFallida()]);
                }
                $user = User::where('id', $user->id)->first();
                return response()->json(['status'=> $this->estadoExitoso(), 'data'=>  $user, 'clave'=>$pass ]);
                 // enviar un correo 
                }    
             }else{
                return response()->json(['status'=> $this->estadoOperacionFallida("No se puede crear usuario Uninorte como externo")]);
             }

        }catch (JWTException $e){
            return $this->toJson($this->estadoOperacionFallida($e));        
        }
     }else{
        return $this->toJson($this->estadoNoAutorizado()); 
     }
    }
    /**
	 * Registro de Usuario externo prueba covid
     * @group usuario externo
    */
    public function registerCodivUser(Request $request){
        $secretToken = config('app.secretToken');
       if($request->tokenApp = $secretToken){
        try{ 
            $rules = [
                'alias'   => 'required',
                'access'  => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if($validator->fails()) {
                return response()->json(['status'=> $this->estadoParametrosIncorrectos(), 'error'=> $validator->messages()]);
            }
            // logs::create(['code'=>87654,'action'=>'Ingreso usuario covid test','message'=>"Usuario covid test ".$request->alias.' Ingreso App'.$request->access,'systemMessage'=>89]);
            $user = userAccessCovid::where('alias',$request->alias)->first();
            if($user){
                // logs::create(['code'=>87654,'action'=>'usuario actualizo el covid test','message'=>"Usuario covid test nuevo ".$request->alias.' Ingreso App'.$request->access,'systemMessage'=>89]);
                $user->access = $request->access;
                $user->save();
                
            }else{
                // logs::create(['code'=>87654,'action'=>'Ingreso usuario covid test nuevo','message'=>"Usuario covid test nuevo ".$request->alias.' Ingreso App'.$request->access,'systemMessage'=>89]);
                $user = userAccessCovid::create(['alias' => $request->alias,'access' =>$request->access]);
            }
            return response()->json(['status'=> $this->estadoExitoso(), 'data'=>  $user ]);
            

        }catch (JWTException $e){
            logs::create(['code'=>87654,'action'=>'Ocurrio un error','message'=>"Error con el Usuario ".$request->alias.' no ingreso'.$request->access,'systemMessage'=>89]);
            return $this->toJson($this->estadoOperacionFallida($e));        
        }
     }else{
        return $this->toJson($this->estadoNoAutorizado()); 
     }
    }
}
