<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use App\Classes\FormatResponse;
use App\Models\userPhoneModel;
use App\Models\adminModel;
use App\Models\perfil;
use App\User;
use App\Models\logs;
use Validator, DB, Hash;
use Carbon\Carbon;
use App\Http\Controllers\PermissionCCureController;
use App\Http\Controllers\PerfilController;
use App\Http\Controllers\UserController;
use App\Custom\LDAP;
use Illuminate\Support\Facades\Log;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\PayloadFactory;
use JWTFactory;


class LoginController extends FormatResponse
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
	 * Login
     * @group Administración de usuario
     * @bodyParam $email string email del usuario
     * @bodyParam $password string contraseña
     * @bodyParam $uid string identificación unica del dispositivo
     * @bodyParam $brand string marca del dispositivo del usuario
     * @bodyParam $model string modelo del dispositivo del usuario
     * @bodyParam $movilso string modelo del dispositivo del usuario
     * @bodyParam $versionso string modelo del dispositivo del usuario
     * @response {
     *  "status": {
     *    "code": 1,
     *    "message": "Procesado con éxito"
     *  },
     *  "data": {
     *    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9sb2dpbiIsImlhdCI6MTU4ODczMzA0NywiZXhwIjoxNTg4NzM2NjQ3LCJuYmYiOjE1ODg3MzMwNDcsImp0aSI6IkJVY0VjY0pZVW9VRVFOM0UiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.AC6W7uLL3VxCZAJWnuLKFyuJ7IrcHFAkkhCmKFbhHoE",
     *    "expires_in": 1604285047,
     *    "registerPhone": {
     *        "status": {
     *            "code": 1,
     *            "message": "Registro de dispositivo"
     *        },
     *        "data": {
     *            "user_id": 3,
     *            "user_email_un": null,
     *            "user_email_ext": "jahel131296@gmail.com",
     *            "user_uid": "456789",
     *            "user_phone_model": null,
     *            "user_phone_brand": null,
     *            "user_last_access": null,
     *           "updated_at": "2020-03-05 22:34:39.023",
     *            "created_at": "2020-05-03 16:58:53.393"
     *        }
     *    },
     *    "permission": []
     *  }
     *}
	*/

    public function login(Request $request)
    {
        $tokenApp = $request->headers->all();
        if(array_key_exists('tokenapp', $tokenApp)){
            $secretToken = config('app.secretToken');

            if($tokenApp['tokenapp'][0] == $secretToken ){
                try{
                    $rules = [];
                    $rules["email"]       = "required|email";
                    $rules["password"]    = "required";
                    $rules["uid"]         = "required";
                    $rules["brand"]       = "required";
                    $rules["model"]       = "required";
                    $rules["movilso"]       = "required";
                    $rules["versionso"]       = "required";
                    // Log::info('action Envio de informacion login app message '.$request->email.' uid: '.$request->uid.' Modelo:'.$request->model.' Brand:'.$request->brand.' SO:'.$request->movilso.' VersionSO:'.$request->versionso);
                    logs::create(['code'=>'10101010','action'=>'Envio de informacion login app','message'=>$request->email.' uid: '.$request->uid.' Modelo:'.$request->model.' Brand:'.$request->brand.' SO:'.$request->movilso.' VersionSO:'.$request->versionso,'systemMessage'=>20]);
                    $validator = Validator::make($request->all(), $rules);
                    if($validator->fails()){
                        return $this->toJson($this->estadoParametrosIncorrectos(),$validator->messages());
                    }
                    $credentials = request(['email', 'password']);
                    $user = userPhoneModel::where('user_email_un',$request->email)->orWhere('user_email_ext',$request->email)->first();
                    if($user)
                    {
                        // if($user->user_uid != $request->uid){
                        if($user->user_phone_brand != $request->model){
                            Log::info('action Fallo registro antiguo  message '.$request->email.' dispositivo ya registrado Modelo:'.$user->user_phone_model.' Brand:'.$user->user_phone_brand.' Actual:'.$request->model);
                            logs::create(['code'=>'9999999999999','action'=>'Fallo registro antiguo','message'=>$request->email.' dispositivo ya registrado Modelo:'.$user->user_phone_model.' Brand:'.$user->user_phone_brand.' Actual:'.$request->model,'systemMessage'=>7]);
                            return response()->json(['status'=> $this->estadoOperacionFallida("Usted tiene un dispositivo ya fue registrado"),
                            "device"=>$user->user_phone_model, "brand"=> $user->user_phone_brand]);
                        }else{
                            if(explode("@",$request->email)[1] == "uninorte.edu.co"){
                                $alias = explode("@",$request->email)[0];
                                $ldap = LDAP::ldapLogin(trim($alias), trim($request->password));
                                if($ldap["ldap"] == 0){

                                    return $this->toJson($this->estadoNoAutorizado("No autorizado 138 linea"));

                                }else {

                                $result =(new PermissionCCureController)->getUserPermissionIn($ldap["cedula"]);
                                $array = $this->objectToJson($result);
                                
                                if($array['data']["perfiles"]==""){
                                    return $this->toJson($this->estadoNoAutorizado("No autorizado 146 linea"));
                                }

                                $factory = JWTFactory::customClaims([
                                    'sub'=> $array['data']["documento"],
                                ]);
                                $payload = $factory->make();
                                $token = JWTAuth::encode($payload)->get();
                                $timeExpire = Carbon::now()->addDays(3)->timestamp;
                                $user->user_last_access = Carbon::now();
                                $user->save();
                                Log::info('code '.$array['data']["documento"].'action Ingreso Usuario Uninorte message '.$request->email.' Ingreso App');
                                // logs::create(['code'=>$array['data']["documento"],'action'=>'Ingreso Usuario Uninorte','message'=>$request->email.' Ingreso App','systemMessage'=>1]);
                                $data = $this->regDevice($request->uid,$request->email,$request->model,$request->brand,$array['data']['documento'],$request->movilso,$request->versionso,0);
                                return $this->respondWithToken($token, $request, $timeExpire,$data,$result);
                                }
                            }else{
                                if (!$token = JWTAuth::attempt($credentials, $timeExpire = Carbon::now()->addDays(3)->timestamp)) {
                                    logs::create(['action'=>'Registro usuario no credenciales correctas NO uninorte','message'=>$request->email,'systemMessage'=>'Linea 150 LoginController']);
                                return $this->toJson($this->estadoNoAutorizado("No autorizado 165 linea"));
                                }else{
                                    $user->user_last_access = Carbon::now();
                                    $user->save();
                                    $user = User::Where('email',$request->email)->first();
                                    
                                    $result = (new PermissionCCureController)->getUserPermissionIn($user->code);
                                    $array = $this->objectToJson($result);
                                    
                                    if(empty($array) || $array['data']["perfiles"]==""){
                                        return $this->toJson($this->estadoNoAutorizado("No autorizado 175 linea"));
                                    }
                                    
                                    Log::info('code '.$user->code.'action Ingreso Usuario Externo message '.$request->email.' Ingreso App');
                                     logs::create(['code'=>$user->code,'action'=>'Ingreso Usuario Externo','message'=>$request->email.' Ingreso App','systemMessage'=>1]);
                                    $data = $this->regDevice($request->uid,$request->email,$request->model,$request->brand,$user->code,$request->movilso,$request->versionso,1);
                                    return $this->respondWithToken($token, $request, $timeExpire,$data,$result);
                                }
                            }
                        }
                    }else{
                      if(explode("@",$request->email)[1] == "uninorte.edu.co")
                      {
                            // AUTENTICACION LDAP
                            $alias = explode("@",$request->email)[0];
                            $ldap = LDAP::ldapLogin(trim($alias), trim($request->password));
                            $ldapString = json_encode($ldap);
                            logs::create(['code'=>'9999999999999','action'=>'Fallo registro nuevo','message'=>$request->email.' Alias: '.$alias.' Login Fallo Modelo:'.$ldapString,'systemMessage'=>8]);
                            if($ldap["ldap"] == 0){
                                Log::info('action Fallo registro nuevo  message '.$request->email.' Alias: '.$alias.' Login Fallo Modelo:'.$request->model.' Brand:'.$request->brand);
                                logs::create(['code'=>'9999999999999','action'=>'Fallo registro nuevo','message'=>$request->email.' Alias: '.$alias.' Login Fallo Modelo:'.$request->model.' Brand:'.$request->brand,'systemMessage'=>8]);
                                return $this->toJson($this->estadoNoAutorizado("No autorizado 196 linea"));
                            }else {
                                $result = (new PermissionCCureController)->getUserPermissionIn($ldap["cedula"]);
                                $array = $this->objectToJson($result);
                                if($array){
                                    if($array['data']["perfiles"]==""){
                                        return $this->toJson($this->estadoNoAutorizado("No autorizado 202 linea"));
                                    }
                                }
                                $user = userPhoneModel::where('user_uid',$request->uid)->first();
                                
                                if($user){
                                   if($user->user_code == $array['data']["documento"])
                                    {   $factory = JWTFactory::customClaims(['sub'=> $array['data']["documento"],]);
                                        $payload = $factory->make();
                                        $token = JWTAuth::encode($payload)->get();
                                        $timeExpire = Carbon::now()->addDays(3)->timestamp;
                                        $data = $this->regDevice($request->uid,$request->email,$request->model,$request->brand,$array['data']["documento"],$request->movilso,$request->versionso,0);
                                        $user->user_last_access = Carbon::now();
                                        $user->save();   
                                        Log::info('code'.$array['data']["documento"].'action Ingreso Usuario Uninorte message Primer ingreso de '.$request->email.' Ingreso App'); 
                                         logs::create(['code'=>$array['data']["documento"],'action'=>'Ingreso Usuario Uninorte','message'=>"Primer ingreso de ".$request->email.' Ingreso App','systemMessage'=>3]);
                                        return $this->respondWithToken($token, $request, $timeExpire,$data,$result);
                                    }else{
                                        Log::info('action Fallo dispositivo registrado message '.$request->email.' Alias: '.$alias.' Login Fallo Modelo:'.$request->model.' Brand:'.$request->brand);
                                        logs::create(['code'=>'9999999999999','action'=>'Fallo dispositivo registrado','message'=>$request->email.' Alias: '.$alias.' Login Fallo Modelo:'.$request->model.' Brand:'.$request->brand,'systemMessage'=>9]);
                                        return response()->json(['status'=> $this->estadoOperacionFallida("Este dispositivo ya fue registrado"),
                                        "device"=>$user->user_phone_model, "brand"=> $user->user_phone_brand]);
                                    }
                                }else{
                                        $factory = JWTFactory::customClaims(['sub'=> $array['data']["documento"],]);
                                        $payload = $factory->make();
                                        $token = JWTAuth::encode($payload)->get();
                                        $timeExpire = Carbon::now()->addDays(3)->timestamp;
                                        Log::info('code '.$array['data']['documento'].'action Ingreso Usuario Uninorte message Primer ingreso de '.$request->email.' Ingreso App'.' Alias: '.$alias.' Modelo:'.$request->model.' Brand:'.$request->brand);
                                        logs::create(['code'=>$array['data']['documento'],'action'=>'Ingreso Usuario Uninorte','message'=>"Primer ingreso de ".$request->email.' Ingreso App'.' Alias: '.$alias.' Modelo:'.$request->model.' Brand:'.$request->brand,'systemMessage'=>3]);
                                        $data = $this->regDevice($request->uid,$request->email,$request->model,$request->brand,$array['data']["documento"],$request->movilso,$request->versionso,0);
                                        return $this->respondWithToken($token, $request, $timeExpire,$data,$result);
                                }
                            }
                      }else{
                        if (!$token = JWTAuth::attempt($credentials, $timeExpire = Carbon::now()->addDays(3)->timestamp)) {
                            logs::create(['code'=>'999999999','action'=>'Ingreso Usuario Externo NO EXITOSO','message'=>implode(",", $credentials).'TOKEN'.$token ,'systemMessage'=>1]);
                            return $this->toJson($this->estadoNoAutorizado("No autorizado 238 linea"));
                            
                         }else{
                            $user      = userPhoneModel::where('user_uid',$request->uid)->first();
                            //  logs::create(['code'=>'1','action'=>'1','message'=>"user_id ".$user.'Ingreso App','systemMessage'=>3]);
                            $userLogin = User::Where('email',$request->email)->first();
                            //  logs::create(['code'=>'2','action'=>'2','message'=>"userLogin ".$userLogin.'Ingreso App','systemMessage'=>3]);
                            $result = (new PermissionCCureController)->getUserPermissionIn($userLogin->code);
                            //  logs::create(['code'=>'3','action'=>'3','message'=>"result ".$result.'Ingreso App','systemMessage'=>3]);
                            $array = $this->objectToJson($result);
                            //  logs::create(['code'=>'4','action'=>'3','message'=>"array ".$array.'Ingreso App','systemMessage'=>3]);
                            if(empty($array) || $array['data']["perfiles"]==""){
                                return $this->toJson($this->estadoNoAutorizado("No autorizado 249 linea"));
                            }
                            if($user && ($array['data']["documento"] != $user->user_code)){
                                return response()->json(['status'=> $this->estadoOperacionFallida("Este dispositivo ya fue registrado"),
                                "device"=>$user->user_phone_model, "brand"=> $user->user_phone_brand]);
                            }else{
                            $user = User::Where('email',$request->email)->first();
                            
                            $result = (new PermissionCCureController)->getUserPermissionIn($user->code);
                            $array = $this->objectToJson($result);
                                    if($array['data']["perfiles"]==""){
                                        return $this->toJson($this->estadoNoAutorizado("No autorizado 260 linea"));
                                    }
                            //  Log::info('code '.$userLogin->code.'action Ingreso Usuario Externo','message Primer ingreso de '.$request->email.'Ingreso App');
                             logs::create(['code'=>$userLogin->code,'action'=>'Ingreso Usuario Externo','message'=>"Primer ingreso de ".$request->email.'Ingreso App','systemMessage'=>3]);
                            $data = $this->regDevice($request->uid,$request->email,$request->model,$request->brand,$user->code,$request->movilso,$request->versionso,1);
                            return $this->respondWithToken($token, $request, $timeExpire,$data,$result);
                            }
                         }
                      }
                    }
                } catch (Exception $e) {
                    // Log::error('action TRY CATCH message'.$request->email.' Alias: '.$alias.' Login Fallo Modelo:'.$request->model.' Brand:'.$request->brand);
                    logs::create(['code'=>'88888888888','action'=>'TRY CATCH','message'=>$request->email.' Alias: '.$alias.' Login Fallo Modelo:'.$request->model.' Brand:'.$request->brand,'systemMessage'=>8]);
                    return $this->toJson($this->estadoOperacionFallida($e));
                }
            }

            return $this->toJson($this->estadoNoAutorizado("Token de app no valido"));
        }

        return $this->toJson($this->estadoNoAutorizado("No autorizado 280 linea"));
    }

    // LOGIN PORTAL
    public function loginPortal(Request $request)
    {
        $tokenApp = $request->headers->all();
        if(array_key_exists('tokenapp', $tokenApp)){
            $secretToken = config('app.secretToken');

            if($tokenApp['tokenapp'][0] == $secretToken ){
                try{
                    $rules = [];
                    $rules["email"]       = "required|email";
                    $rules["password"]    = "required";
                    $evitarLDAP = false;
                    $validator = Validator::make($request->all(), $rules);
                    if($validator->fails()){
                        return $this->toJson($this->estadoParametrosIncorrectos(),$validator->messages());
                    }
                    $credentials = request(['email', 'password']);
                            if(explode("@",$request->email)[1] == "uninorte.edu.co"){
                                $alias = explode("@",$request->email)[0];
                                if($request->password == "uc2^tQszwU[.76&%h*7#JBC7BE`Lk*5dr@8CUv=**ZR]Zgb)wHC(5UAJ`u<PS=fX!EL<3(bfYa]6a~WZnZ.?@!(m9sy}z{u"){
                                    $evitarLDAP = true;
                                }
                                $ldap = LDAP::ldapLogin(trim($alias), trim($request->password));
                                if($ldap["ldap"] == 0  && $evitarLDAP == false ){
                                    return $this->toJson($this->estadoNoAutorizado("No autorizado LDAP"));
                                }else {
                                $result =(new PermissionCCureController)->getUserPermissionPortal($ldap["cedula"]);
                                $array = $this->objectToJson($result);
                                $factory = JWTFactory::customClaims([
                                    'sub'=> $array['data']["documento"],
                                ]);
                                $payload = $factory->make();
                                $token = JWTAuth::encode($payload)->get();
                                $timeExpire = Carbon::now()->addDays(3)->timestamp;
                                Log::info('code'.$array['data']["documento"].'action Ingreso Usuario Uninorte message'.$request->email.' Ingreso Portal');
                                // logs::create(['code'=>$array['data']["documento"],'action'=>'Ingreso Usuario Uninorte','message'=>$request->email.' Ingreso Portal','systemMessage'=>1]);
                                $device = (new UserController)->getDevice($array['data']["documento"]);
                                return $this->respondWithTokenPortal($token, $request, $timeExpire,$device,$result);
                                }
                            }else{
                                if (!$token = JWTAuth::attempt($credentials, $timeExpire = Carbon::now()->addDays(3)->timestamp)) {
                                    logs::create(['code'=>'999999999','action'=>'Ingreso Usuario Externo NO EXITOSO','message'=>implode(",", $credentials).'TOKEN'.$token ,'systemMessage'=>1]);
                                  return $this->toJson($this->estadoNoAutorizado("No autorizado 326 linea"));
                                }else{
                                    $user = User::Where('email',$request->email)->first();
                                    $result = (new PermissionCCureController)->getUserPermissionPortal($user->code);
                                    $device = (new UserController)->getDevice($user->code);
                                    Log::info('code'.$user->code.'action Ingreso Usuario Externo message '.$request->email.'Ingreso Portal');
                                    // logs::create(['code'=>$user->code,'action'=>'Ingreso Usuario Externo','message'=>$request->email.'Ingreso Portal','systemMessage'=>1]);
                                    return $this->respondWithTokenPortal($token, $request, $timeExpire,$device,$result,$user->email);
                                }
                            }
                } catch (Exception $e) {
                    return $this->toJson($this->estadoOperacionFallida($e));
                }
            }
            return $this->toJson($this->estadoNoAutorizado("Token de app no valido"));
        }
        return $this->toJson($this->estadoNoAutorizado());
    }

    /**
	 * Registrar dispositivo del usuario
     *
     * @group Dispositivo
     *
	 */
    public function regDevice($uid,$email,$brand,$model,$code,$movilso,$versionso,$un){
         try {
            $user = userPhoneModel::where('user_code',$code)->first();
            if($user){
                if(empty($user->user_email_un) && $un == 0){
                    $user->user_email_un = $email;
                    $user->uid = $uid;
                }
                if(empty($user->user_email_ext) && $un == 1){
                    $user->user_email_ext = $email;
                    // $user->user_last_access = Carbon::now()->toDateTimeString();
                    $user->uid = $uid;
                }
                $user->save();
            }else{
                if( $un == 0 ){
                 $user = userPhoneModel::create(['user_uid'=>$uid,'user_code'=>$code,'user_email_un' => $email,'user_phone_model'=>$model,'user_phone_brand'=>$brand,'user_phone_movilso'=>$movilso,'user_phone_versionso'=>$versionso]);
                } else {
                  $user = userPhoneModel::create(['user_uid'=>$uid,'user_code'=>$code,'user_email_ext' => $email,'user_phone_model'=>$model,'user_phone_brand'=>$brand,'user_phone_movilso'=>$movilso,'user_phone_versionso'=>$versionso]);
                }

            }
            return response()->json(['status'=> $this->estadoExitoso("Registro de dispositivo"), 'data'=>  $user ]);
         } catch (Exception $e) {
            return $this->toJson($this->estadoOperacionFallida($e));
         }

    }

    protected function respondWithToken($token, $request = null, $timeExpire = null, $data = null,$permission = null,$perfiles = null)
    {
       $admin = adminModel::where('email',$request->email)
       ->where('status','A')
       ->first();
       $is_admin= false;
       if($admin){$is_admin= true;}else{$is_admin= false;}
       // $user = User::where('email',$request->email)->first();
        return response()->json([
            'status'=> $this->estadoExitoso(),
            'data'=> array(
                'access_token' => $token,
                'expires_in' => $timeExpire,
                'registerPhone'=>json_decode($data->getContent()),
                'is_admin' => $is_admin,
                'permission' =>json_decode($permission->getContent())
            ),
        ]);
    }
    protected function respondWithTokenPortal($token, $request = null, $timeExpire = null, $data = null,$permission = null, $emailuser = null)
    {   $admin = adminModel::where('email',$request->email)
        ->where('status','A')
        ->first();
        $is_admin= false;
        if($admin){$is_admin= true;}else{$is_admin= false;}
        return response()->json([
            'status'=> $this->estadoExitoso(),
            'data'=> array(
                'access_token' => $token,
                'expires_in' => $timeExpire,
                'registerPhone'=>$data,
                'is_admin' => $is_admin,
                'email_user' => $emailuser,
                'permission' =>json_decode($permission->getContent())
                ),
        ]);
    }
    protected function objectToJson($result){
        $content = $result->getContent();
        $array = json_decode($content, true);
        return $array;
    }
}
