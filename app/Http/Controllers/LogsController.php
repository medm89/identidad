<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\FormatResponse;
use App\Models\userPhoneModel;
use App\User;
use Validator, DB, Hash, Mail;
use App\Models\logs;
use App\Models\qr;
use App\Models\systemConfigModel;
use App\Models\systemConfigEnvModel;
use Illuminate\Contracts\Encryption\DecryptException;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class LogsController extends FormatResponse
{
    public function  __construct(){
        $this->middleware('cors');
    }

    /**
	 * Registro de Usuario externo
     * @group Administración de usuario
    */
    public function registerLog(Request $request){
        $secretToken = config('app.secretToken');
        if($request->tokenApp = $secretToken){
        try{ 
            $rules = [
                'code'     => 'required|max:255',
                'action'  => 'required',
                'message'  => 'required'
            ];
            $validator = Validator::make($request->all(), $rules);
            if($validator->fails()) {
                return response()->json(['status'=> $this->estadoParametrosIncorrectos(), 'error'=> $validator->messages()]);
            }
            Log::info('code '.$request->code.' action '.$request->action.' message '.$request->message);
            // logs::create(['code'=>$request->code,'action'=>$request->action,'message'=>$request->message,'systemMessage'=>6]);
            return $this->toJson($this->estadoExitoso());

        }catch (JWTException $e){
            return $this->toJson($this->estadoOperacionFallida($e));        
        }
     }else{
        return $this->toJson($this->estadoNoAutorizado()); 
     }
    }

    public function deCodeQR(Request $request){
        $secretToken = config('app.secretToken');
        if($request->tokenApp = $secretToken){
        try{ 
            $rules = [
                'code'     => 'required'
            ];
            $validator = Validator::make($request->all(), $rules);
            if($validator->fails()) {
                return response()->json(['status'=> $this->estadoParametrosIncorrectos(), 'error'=> $validator->messages()]);
            }
            $qr = qr::where('secret',$request->code)->first();
            Log::info(' action '.' decodifica qr '.' message '.$request->code);
            // logs::create(['code'=>88888876543,'action'=>'decodifica qr','message'=>$request->code,'systemMessage'=>31]);
            if($qr){
              $carbon1 = Carbon::now();
              $carbon2 =Carbon::createFromFormat('Y-m-d H:i:s',$qr->updated_at);
              $minutesDiff=$carbon1->diffInSeconds($carbon2);
            
            //   logs::create(['code'=>88888876543,'action'=>'decodifica qr'.$request->code,'message'=>$qr->code." Diff min ".$minutesDiff. " hora actual ".$carbon1." Hora BD ".$carbon2,'systemMessage'=>30]);
                $systemConfig = systemConfigModel::select('options')->first();
                $timeQR  =   intval($systemConfig->options);
                // logs::create(['code'=>88888876543,'action'=>'decodifica qr'.$request->code,'message'=>$qr->code." Diff seconds ".$minutesDiff. " hora actual ".$carbon1." Hora BD ".$carbon2." TimeQR".$timeQR,'systemMessage'=>30]);
                Log::info('action  decodifica qr '.$request->code.' message '.$qr->code." Diff min ".$minutesDiff. " hora actual ".$carbon1." Hora BD ".$carbon2);
            if($qr->flag == 1 ){
                if($minutesDiff <= ($timeQR + 10) ){
                    $qr->flag = 0;
                    $qr->save();
                    return response()->json(['status'=> $this->estadoExitoso(), 'acceso'=>$qr  ]);
                }else{
                    return response()->json(['status'=> $this->estadoOperacionFallida("Este codigo ya fue utilizado o no existe"), 'acceso'=>'negado'  ]);
                }
                
            }else{
               return response()->json(['status'=> $this->estadoOperacionFallida("Este codigo ya fue utilizado o no existe"), 'acceso'=>'negado'  ]);
            }


            }else{
               
            }
        }catch (JWTException $e){
            return $this->toJson($this->estadoOperacionFallida($e));        
        }
     }else{
        return $this->toJson($this->estadoNoAutorizado()); 
     }
    }

    public function getTimeQR(Request $request){
        $secretToken = config('app.secretToken');
        if($request->tokenApp = $secretToken){
        try{ 
            
            $systemConfig = systemConfigModel::all();
            // logs::create(['code'=>88888876543,'action'=>'decodifica qr','message'=>$request->code,'systemMessage'=>31]);
            if($systemConfig){
                return response()->json(['status'=> $this->estadoExitoso(), 'tiempo'=>$systemConfig  ]);
            }else{
               return response()->json(['status'=> $this->estadoOperacionFallida("No se encontro configuracion del servidor"), 'tiempo'=>'negado'  ]);
            }

        }catch (JWTException $e){
            return $this->toJson($this->estadoOperacionFallida($e));        
        }
     }else{
        return $this->toJson($this->estadoNoAutorizado()); 
     }
    }

    public function getRemoteConfig(Request $request){
        $secretToken = config('app.secretToken');
        if($request->tokenApp = $secretToken){
        try{ 
            
            $systemConfig = systemConfigEnvModel::all();
            // logs::create(['code'=>88888876543,'action'=>'decodifica qr','message'=>$request->code,'systemMessage'=>31]);
            if($systemConfig){
                return response()->json(['status'=> $this->estadoExitoso(), 'data'=>$systemConfig  ]);
            }else{
               return response()->json(['status'=> $this->estadoOperacionFallida("No se encontro configuracion del servidor"), 'tiempo'=>'negado'  ]);
            }

        }catch (JWTException $e){
            return $this->toJson($this->estadoOperacionFallida($e));        
        }
     }else{
        return $this->toJson($this->estadoNoAutorizado()); 
     }
    }

    protected function objectToJson($result){
        $content = $result->getContent();
        $array = json_decode($content, true);
        return $array;
    }
}
