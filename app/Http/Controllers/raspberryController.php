<?php

namespace App\Http\Controllers;

use JWT;
use App\Models\raspberry;
use Illuminate\Http\Request;
use App\Classes\FormatResponse;
use Input;
use Validator, DB, Hash, Mail; 

class raspberryController extends FormatResponse
{
    public function  __construct(){
        $this->middleware('cors'); 
        $this->middleware('jwt');
    }

    /**
	   * Crear raspberry nuevas
     * @authenticated
     * @group Preguntas Frecuentes
     * 
	  */ 
      public function createRaspberry (Request $request){
        $secretToken = config('app.secretToken');
          if($request->tokenApp = $secretToken){
          try{ 
              $rules = [
                  'code'  => 'required',
                  'name'  => 'required'
              ];
              $validator = Validator::make($request->all(), $rules);
              if($validator->fails()) {
                  return response()->json(['status'=> $this->estadoParametrosIncorrectos(), 'error'=> $validator->messages()]);
              }
              $raspberry = raspberry::where('code',$request->code)->first();
              if($raspberry){
                return response()->json(['status'=> $this->estadoOperacionFallida("Este codigo ya existe"), 'data'=>$raspberry  ]);
              }else{
                $raspberry = raspberry::create(['code'=>$request->code,'name'=>$request->name]);
                return $this->toJson(['status'=> $this->estadoExitoso(), 'data'=>  $raspberry ]);    
              }
          }catch (JWTException $e){
              return $this->toJson($this->estadoOperacionFallida($e));        
          }
       }else{
          return $this->toJson($this->estadoNoAutorizado()); 
       }
    }

    public function getRaspberry(Request $request){
        $secretToken = config('app.secretToken');
        if($request->tokenApp = $secretToken){
                $raspberry = raspberry::orderBy('created_at', 'DESC')
                ->get();
          return $this->toJson($this->estadoExitoso(),$raspberry);
        }
        return $this->toJson($this->estadoNoAutorizado("Token de app no valido"));  
   }

    public function delRaspberry(Request $request){
        $secretToken = config('app.secretToken');
          if($request->tokenApp = $secretToken){
              $raspberry = raspberry::where('id',$request->id)->delete();  //::where('code', $request->code)->first();
              $raspberry = raspberry::All();
               return response()->json(['status'=> $this->estadoExitoso(), 'data'=>  $raspberry ]);
          }
        return $this->toJson($this->estadoNoAutorizado("Token de app no valido"));
      }

      public function updateRaspberry (Request $request){
          $secretToken = config('app.secretToken');
            if($request->tokenApp = $secretToken){
            try{ 
                $rules = [
                    'name'  => 'required',
                    'code'   => 'required',
                    'id'       => 'required',
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails()) {
                    return response()->json(['status'=> $this->estadoParametrosIncorrectos(), 'error'=> $validator->messages()]);
                }
                $raspberry = raspberry::where('id',$request->id)->first();
                if($raspberry){
                  $raspberry->code   =  $request->code;
                  $raspberry->name   =  $request->name;
                  
                }
                $raspberry->save();
                return response()->json(['status'=> $this->estadoExitoso(), 'data'=>  $raspberry ]);
    
            }catch (JWTException $e){
                return $this->toJson($this->estadoOperacionFallida($e));        
            }
         }else{
            return $this->toJson($this->estadoNoAutorizado()); 
         }
        }
}
