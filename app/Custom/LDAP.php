<?php

namespace App\Custom;
/**
 * Description of LDAP
 *
 * @author Jairo Pimentel
 */

use Illuminate\Support\Facades\Log;

class LDAP {
    
    public static function ldapLogin($user, $password){
        //include('config.php.inc');

        $user=strtolower($user);

	//validad que el usuario existe en ldap, llenas las variables, rol, documento y el Booleano de si las credenciales son correctas en ldap
	try {
            $datos_usuario=LDAP::ldap($user,$password,1);
            $v_datos_usuario=explode(";",$datos_usuario);
            
            return array('ldap' => $v_datos_usuario[0], 'correo' => $v_datos_usuario[1], 'cedula' => $v_datos_usuario[2], 'fullname' => $v_datos_usuario[3]);
	} catch (Exception $e) {
		$ldap="0";
                Log::error($e->getMessage());
        }
        
        return "Error ldpa";
    }
    
    public static function ldap($usuario,$clave,$tipo_consulta)
    {   
	$encontrado=false;
	$autenticar="0";
	$correo="0";
	$cedula="0";
	$cnname="0";
	$tarifas_especiales="0";
	$Faculty="0";
	$Employee="0";
	$Student="0";	
        
        /*ldap*/
        $ldapuser = 'uid=lector,ou=People,o=uninorte.edu.co,o=uninorte.edu.co';
        $ldapclave = 'UNl3ct0rApp';
        $ldaphost = "172.16.11.149";  
        $ldapport = 389;
	
	//$config_file='config.php.inc';
	//include($config_file);
	if($usuario!="" && $clave!=""){		
	
            $autenticar=0;
            $tarifas_especiales= 0;
            $Faculty= 0;
            $Employee= 0;
            $Student=0;
            $correo=0;
            $cedula=0;
            $cnname=0;	

            $ldapconn = ldap_connect($ldaphost, $ldapport);
            if ($ldapconn){
			
                $ldapbind = @ldap_bind($ldapconn, $ldapuser, $ldapclave);			
                if ($ldapbind && $ldapclave!=NULL) {	
                    $dn = array("ou=people,o=uninorte.edu.co,o=uninorte.edu.co","ou=personas,o=uninorte.edu.co,o=uninorte.edu.co","ou=personas,o=web,o=uninorte.edu.co");
                    $recorrido=0;
                    $found = false;
                    while($recorrido<2 && $found == false){
                        $filter="uid=".$usuario;
                        $sr=ldap_search($ldapconn, $dn[$recorrido],$filter);								
                        $info = ldap_get_entries($ldapconn, $sr);					
                        if ($info["count"]>0){
                            $found = true;
                        }else{
                            $recorrido = $recorrido+1;
                        }
                    }
                }
			
                if ($found){

                    $ldapuser = 'uid=' . $usuario. ','.$dn[$recorrido];
                    $ldapclave = $clave;    
                    $ldapbind = @ldap_bind($ldapconn, $ldapuser, $ldapclave);
                    
                    $correo = $info[0]["mail"][0]."@uninorte.edu.co";
                    $cedula = $info[0]["employeenumber"][0];					
                    $cnname = $info[0]["displayname"][0];

                    if ($ldapbind && $ldapclave!=NULL) {				
                        $encontrado=true;											
                        $autenticar=1;	
                        //Recuperar datos del LDAP
                        $correo = $info[0]["mail"][0]."@uninorte.edu.co"; 			
                        //$cedula= $info[0]["cn"][0];
                        $cedula = $info[0]["employeenumber"][0];					
                        $cnname = $info[0]["displayname"][0];
                        $contador=0;
                        //Consultar roles				

                            //captura el rol en LM5 -- Esperar que habiliten permiso de conexi n 
                            /*if($tipo_consulta==1) {
                                $rol ="";
                                //$conn = oci_connect($dbuser_luminis, $dbpassword_luminis,$dbdsn_luminis);
                                $conn = oci_connect($dbuser_luminis,$dbpassword_luminis,$portal_conn_string);
                                if($conn){
                                    $query = "SELECT PACK_CONSULTAS.FUN_CONSULTA_ROL($cedula) FROM dual";
                                    $stid = oci_parse($conn, $query);
                                    $result=oci_execute($stid);			
                                    $row = oci_fetch_array($stid);
                                    $rol=$row[0];
                                    $tempo="";
                                    /*while($row = oci_fetch_array($stid)){
                                            $tempo .= "<p class='realrestult'>".var_dump($row)."</p>";
                                            $rol=$row[0];
                                    }*/	
                                    /*oci_close($conn );
                                }else{
                                    echo "<br/>--error de conexion--<br/>"; 
                                    $m = oci_error(); 
                                    echo $m['message'], "\n"; 
                                    exit; 
                                }

                                if($rol!=""){	
                                    //preg_match indica si una cadena est  en otra
                                    if(preg_match("/FACULTY/",$rol))
                                            $Faculty=1;				
                                    if((preg_match("/EMPLOYEE/",$rol)) || (preg_match("/Empleado/",$rol)))
                                            $Employee=1;		
                                    if(preg_match("/STUDENT/",$rol))	
                                            $Student=1;		
                                }
                                //fin consultar_rol
                            }			
                        //$cedula=$cedula."::sct";*/
                        
                    }
                }
                ldap_close($ldapconn);
                
                //dd([$correo, $info[0]["employeenumber"][0], $cnname]);
            }
	}		

	if($tipo_consulta==0) return $encontrado;
	//if($tipo_consulta==1) return ($autenticar.";".$correo.";".$cedula.";".$cnname.";".$tarifas_especiales.";".$Faculty.";".$Employee.";".$Student.";".$rol.";".$tempo);
        if($tipo_consulta==1) return ($autenticar.";".$correo.";".$cedula.";".$cnname);
    }
}
