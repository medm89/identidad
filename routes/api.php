<?php

// use Illuminate\Http\Request;
// use App\Classes\FormatResponse;
//use Validator;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::POST('login', 'Auth\LoginController@login')->name('login');
Route::POST('loginPortal', 'Auth\LoginController@loginPortal')->name('loginPortal');

Route::POST('getFaqs', 'FaqsController@getFaqs');
Route::POST('registerExtUserIn'  , 'RegisterUserInController@registerExtUserIN');

Route::POST('registerLog'  , 'LogsController@registerLog'); 
Route::POST('deCodeQR'  , 'LogsController@deCodeQR');
Route::GET('getTimeQR'  , 'LogsController@getTimeQR'); 
Route::GET('getRemoteConfig'  , 'LogsController@getRemoteConfig'); 
Route::group(['middleware' => ['cors']], function () {
    Route::POST('registerCodivUser'  , 'RegisterUserInController@registerCodivUser');
});

//----------------------------- RUTAS QUE NECESITAN LOGIN -----------------------------
Route::group(['prefix' => 'auth'], function() {

   Route::GET('/logout', 'AuthController@logout');
   Route::GET('getUserPermission','PermissionCCureController@getUserPermission');
   Route::POST('encodeQR'       , 'UserController@encodeQR');
   Route::POST('activateCovidTest'       , 'UserController@activateCovidTest');
   Route::GET('getCovidTest'       , 'UserController@getCovidTest');
//____________________________________  ADMINISTRADOR _____________________________________

    Route::GET('profile'       , 'UserController@profile');
    Route::POST('validateUserAccess'       , 'UserController@validateUserAccess');
    Route::GET('getAllRegisterDevice'  , 'UserController@getAllRegisterDevice');
    Route::POST('getAllUser'  , 'UserController@getAllUser');
    Route::POST('registerExtUser'  , 'RegisterUserController@registerExtUser');
    Route::POST('permissionAdmin'  , 'UserController@permissionAdmin');
    Route::POST('changePassword'  , 'UserController@changePassword');
    Route::POST('sendRequestCard'  , 'UserController@sendRequestCard');
    Route::POST('sendRequestCardTest'  , 'UserController@sendRequestCardTest');
    Route::POST('registerAdmUser'  , 'RegisterUserController@registerAdmUser');
    Route::POST('delAdmin'  , 'UserController@delAdmin');
    Route::GET('getAllAdmin','UserController@getAllAdmin');

    Route::POST('getUserAccess','UserController@getUserAccess');
    Route::POST('getReportUsers','UserController@getReportUsers');

    Route::POST('getInfoUser','UserController@getInfoUser');
    Route::POST('getInfoUserByUserName','UserController@getInfoUserByUserName');
    Route::POST('delInfoUser','UserController@delInfoUser');
    Route::POST('changeEmail','UserController@changeEmail');
    Route::POST('createUserExt','UserController@createUserExt');
    

    Route::GET('getRaspberry'       , 'raspberryController@getRaspberry');
    Route::POST('createRaspberry'   , 'raspberryController@createRaspberry');
    Route::POST('delRaspberry'      , 'raspberryController@delRaspberry');
    Route::POST('updateRaspberry'   , 'raspberryController@updateRaspberry');

//_____________________________________ Dispositivo ---------------------------------------
    Route::POST('getRegisterDevice'  , 'UserController@getRegisterDevice');
    Route::POST('unlinkDevice'  , 'UserController@unlinkDevice');

    Route::POST('uploadPhoto'  , 'UserController@uploadPhoto');
    Route::POST('photoStates'  , 'UserController@photoStates');

    Route::POST('createFaqs', 'FaqsController@createFaqs');
    Route::POST('delFaqs', 'FaqsController@delFaqs');
    Route::POST('updateFaqs', 'FaqsController@updateFaqs');

    Route::GET('getPerfil', 'PerfilController@getPerfil');
    Route::POST('createPerfil', 'PerfilController@createPerfil');
    Route::POST('delPerfil', 'PerfilController@delPerfil');
    Route::POST('updatePerfil', 'PerfilController@updatePerfil');


});
