---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](127.0.0.0/api/docs/collection.json)

<!-- END_INFO -->

#Administración de usuario


<!-- START_c3fa189a6c95ca36ad6ac4791a873d23 -->
## Login

> Example request:

```bash
curl -X POST \
    "127.0.0.0/api/api/login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -H "tokenApp: YOUR_API_KEY_HERE" \
    -d '{"$email":"distinctio","$password":"et","$uid":"sed","$brand":"consequatur","$model":"dignissimos","$movilso":"commodi","$versionso":"eius"}'

```

```javascript
const url = new URL(
    "127.0.0.0/api/api/login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
    "tokenApp": "YOUR_API_KEY_HERE",
};

let body = {
    "$email": "distinctio",
    "$password": "et",
    "$uid": "sed",
    "$brand": "consequatur",
    "$model": "dignissimos",
    "$movilso": "commodi",
    "$versionso": "eius"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "status": {
        "code": 1,
        "message": "Procesado con éxito"
    },
    "data": {
        "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9sb2dpbiIsImlhdCI6MTU4ODczMzA0NywiZXhwIjoxNTg4NzM2NjQ3LCJuYmYiOjE1ODg3MzMwNDcsImp0aSI6IkJVY0VjY0pZVW9VRVFOM0UiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.AC6W7uLL3VxCZAJWnuLKFyuJ7IrcHFAkkhCmKFbhHoE",
        "expires_in": 1604285047,
        "registerPhone": {
            "status": {
                "code": 1,
                "message": "Registro de dispositivo"
            },
            "data": {
                "user_id": 3,
                "user_email_un": null,
                "user_email_ext": "jahel131296@gmail.com",
                "user_uid": "456789",
                "user_phone_model": null,
                "user_phone_brand": null,
                "user_last_access": null,
                "updated_at": "2020-03-05 22:34:39.023",
                "created_at": "2020-05-03 16:58:53.393"
            }
        },
        "permission": []
    }
}
```

### HTTP Request
`POST api/login`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `$email` | string |  optional  | email del usuario
        `$password` | string |  optional  | contraseña
        `$uid` | string |  optional  | identificación unica del dispositivo
        `$brand` | string |  optional  | marca del dispositivo del usuario
        `$model` | string |  optional  | modelo del dispositivo del usuario
        `$movilso` | string |  optional  | modelo del dispositivo del usuario
        `$versionso` | string |  optional  | modelo del dispositivo del usuario
    
<!-- END_c3fa189a6c95ca36ad6ac4791a873d23 -->

<!-- START_7bbca3c3b8628808605f9bf16f8c7d9b -->
## Registro de Usuario externo

> Example request:

```bash
curl -X POST \
    "127.0.0.0/api/api/registerExtUserIn" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -H "tokenApp: YOUR_API_KEY_HERE"
```

```javascript
const url = new URL(
    "127.0.0.0/api/api/registerExtUserIn"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
    "tokenApp": "YOUR_API_KEY_HERE",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/registerExtUserIn`


<!-- END_7bbca3c3b8628808605f9bf16f8c7d9b -->

<!-- START_16928cb8fc6adf2d9bb675d62a2095c5 -->
## Cerrar sesión

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "127.0.0.0/api/api/auth/logout" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -H "tokenApp: YOUR_API_KEY_HERE"
```

```javascript
const url = new URL(
    "127.0.0.0/api/api/auth/logout"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
    "tokenApp": "YOUR_API_KEY_HERE",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "status": {
        "code": 1,
        "message": "Procesado con éxito"
    }
}
```

### HTTP Request
`GET api/auth/logout`


<!-- END_16928cb8fc6adf2d9bb675d62a2095c5 -->

<!-- START_582029d77fe82b484cb28254bcd965c2 -->
## Registro de Usuario externo

> Example request:

```bash
curl -X POST \
    "127.0.0.0/api/api/auth/registerExtUser" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -H "tokenApp: YOUR_API_KEY_HERE"
```

```javascript
const url = new URL(
    "127.0.0.0/api/api/auth/registerExtUser"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
    "tokenApp": "YOUR_API_KEY_HERE",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/auth/registerExtUser`


<!-- END_582029d77fe82b484cb28254bcd965c2 -->

<!-- START_61e43df9e4f4316f250f81b54be1ae51 -->
## Asignar permiso a administrador

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "127.0.0.0/api/api/auth/permissionAdmin" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -H "tokenApp: YOUR_API_KEY_HERE" \
    -d '{"$code":16,"$is_admin":7}'

```

```javascript
const url = new URL(
    "127.0.0.0/api/api/auth/permissionAdmin"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
    "tokenApp": "YOUR_API_KEY_HERE",
};

let body = {
    "$code": 16,
    "$is_admin": 7
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "status": {
        "code": 1,
        "message": "Procesado con éxito"
    }
}
```
> Example response (200):

```json
{
    "status": {
        "code": -1,
        "message": "No se encuentra al usuario"
    }
}
```
> Example response (200):

```json
{
    "status": {
        "code": 401,
        "message": "Token de app no valido"
    }
}
```
> Example response (200):

```json
{
    "status": {
        "code": 401,
        "message": "No autorizado"
    }
}
```

### HTTP Request
`POST api/auth/permissionAdmin`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `$code` | integer |  optional  | cedula de usuario
        `$is_admin` | integer |  optional  | 1: es administrador, 0: no es administrador.
    
<!-- END_61e43df9e4f4316f250f81b54be1ae51 -->

<!-- START_0da2ab246e9fccf093fed19f7388683c -->
## Cambiar contraseña de usuario

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "127.0.0.0/api/api/auth/changePassword" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -H "tokenApp: YOUR_API_KEY_HERE" \
    -d '{"$email":18,"$password":"et"}'

```

```javascript
const url = new URL(
    "127.0.0.0/api/api/auth/changePassword"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
    "tokenApp": "YOUR_API_KEY_HERE",
};

let body = {
    "$email": 18,
    "$password": "et"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "status": {
        "code": 1,
        "message": "Procesado con éxito"
    }
}
```
> Example response (200):

```json
{
    "status": {
        "code": -1,
        "message": "No se encuentra el email"
    }
}
```
> Example response (200):

```json
{
    "status": {
        "code": 401,
        "message": "Token de app no valido"
    }
}
```
> Example response (200):

```json
{
    "status": {
        "code": 401,
        "message": "No autorizado"
    }
}
```

### HTTP Request
`POST api/auth/changePassword`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `$email` | integer |  optional  | email del usuario
        `$password` | string |  optional  | Nueva contraseña
    
<!-- END_0da2ab246e9fccf093fed19f7388683c -->

<!-- START_35830c3227aaf43d0fc99419764e8488 -->
## Enviar petición de tarjeta

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "127.0.0.0/api/api/auth/sendRequestCard" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -H "tokenApp: YOUR_API_KEY_HERE" \
    -d '{"$message":"molestiae"}'

```

```javascript
const url = new URL(
    "127.0.0.0/api/api/auth/sendRequestCard"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
    "tokenApp": "YOUR_API_KEY_HERE",
};

let body = {
    "$message": "molestiae"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "status": {
        "code": 1,
        "message": "Procesado con éxito"
    }
}
```
> Example response (200):

```json
{
    "status": {
        "code": 401,
        "message": "Token de app no valido"
    }
}
```
> Example response (200):

```json
{
    "status": {
        "code": 401,
        "message": "No autorizado"
    }
}
```

### HTTP Request
`POST api/auth/sendRequestCard`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `$message` | string |  optional  | mensaje que envia el usuario
    
<!-- END_35830c3227aaf43d0fc99419764e8488 -->

<!-- START_4c4da6f0fe5252bcb0dbc9b737d9355b -->
## Subir foto del usuario

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "127.0.0.0/api/api/auth/uploadPhoto" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -H "tokenApp: YOUR_API_KEY_HERE" \
    -d '{"$code":8,"$image":"exercitationem"}'

```

```javascript
const url = new URL(
    "127.0.0.0/api/api/auth/uploadPhoto"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
    "tokenApp": "YOUR_API_KEY_HERE",
};

let body = {
    "$code": 8,
    "$image": "exercitationem"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "status": {
        "code": 1,
        "message": "Procesado con éxito"
    },
    "data": {
        "state": "",
        "response": 500,
        "error": "Archivo Incorrecto",
        "message": "ERROR"
    }
}
```
> Example response (200):

```json
{
    "status": {
        "code": 1,
        "message": "Procesado con éxito"
    },
    "data": {
        "response": 200,
        "message": "OK",
        "state": {
            "DO": {
                "id": 9030,
                "perfil": "DO",
                "message": "OK"
            },
            "EG": {
                "id": 9031,
                "perfil": "EG",
                "message": "OK"
            }
        }
    }
}
```
> Example response (200):

```json
{
    "status": {
        "code": 1,
        "message": "Procesado con éxito"
    },
    "data": {
        "response": 200,
        "message": "OK",
        "state": {
            "DO": {
                "message": "ERROR",
                "error": "Ya hay una solicitud creada",
                "perfil": "DO"
            },
            "EG": {
                "message": "ERROR",
                "error": "Ya hay una solicitud creada",
                "perfil": "EG"
            }
        }
    }
}
```
> Example response (200):

```json
{
    "status": {
        "code": -1,
        "message": "No se selecciono una foto"
    }
}
```
> Example response (200):

```json
{
    "status": {
        "code": -1,
        "message": "Error: Operación fallida"
    }
}
```

### HTTP Request
`POST api/auth/uploadPhoto`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `$code` | integer |  optional  | cedula de usuario
        `$image` | file |  optional  | imagen de perfil
    
<!-- END_4c4da6f0fe5252bcb0dbc9b737d9355b -->

<!-- START_b0cb997eaae4b81f83c336f48daeb9c7 -->
## Ver estado foto

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "127.0.0.0/api/api/auth/photoStates" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -H "tokenApp: YOUR_API_KEY_HERE" \
    -d '{"$code":10}'

```

```javascript
const url = new URL(
    "127.0.0.0/api/api/auth/photoStates"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
    "tokenApp": "YOUR_API_KEY_HERE",
};

let body = {
    "$code": 10
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "status": {
        "code": 1,
        "message": "Procesado con éxito"
    },
    "data": {
        "state": "Pendiente",
        "response": 200,
        "message": "Estado Consultado Correctamente"
    }
}
```
> Example response (200):

```json
{
    "status": {
        "code": 1,
        "message": "Procesado con éxito"
    },
    "data": {
        "state": "",
        "response": 200,
        "message": "No hay Fotos Registradas"
    }
}
```

### HTTP Request
`POST api/auth/photoStates`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `$code` | integer |  optional  | cedula de usuario
    
<!-- END_b0cb997eaae4b81f83c336f48daeb9c7 -->

#Dispositivo


<!-- START_db8428543691bcd64621481a5576eb14 -->
## Obtener dispositivos del usuario

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "127.0.0.0/api/api/auth/getRegisterDevice" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -H "tokenApp: YOUR_API_KEY_HERE" \
    -d '{"$email":"ullam"}'

```

```javascript
const url = new URL(
    "127.0.0.0/api/api/auth/getRegisterDevice"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
    "tokenApp": "YOUR_API_KEY_HERE",
};

let body = {
    "$email": "ullam"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "status": {
        "code": 1,
        "message": "Procesado con éxito"
    },
    "data": {
        "user_id": 3,
        "user_email_un": null,
        "user_email_ext": "jahel131296@gmail.com",
        "user_uid": "456789",
        "user_phone_model": null,
        "user_phone_brand": null,
        "user_last_access": null,
        "updated_at": "2020-03-05 22:34:39.023",
        "created_at": "2020-05-03 16:58:53.393"
    }
}
```

### HTTP Request
`POST api/auth/getRegisterDevice`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `$email` | string |  optional  | email del usuario
    
<!-- END_db8428543691bcd64621481a5576eb14 -->

<!-- START_cc5af75259b39025f65ab5d9bfb89e87 -->
## Desvincular dispositivo

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X POST \
    "127.0.0.0/api/api/auth/unlinkDevice" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -H "tokenApp: YOUR_API_KEY_HERE" \
    -d '{"$email":"minus"}'

```

```javascript
const url = new URL(
    "127.0.0.0/api/api/auth/unlinkDevice"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
    "tokenApp": "YOUR_API_KEY_HERE",
};

let body = {
    "$email": "minus"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "status": {
        "code": 1,
        "message": "Procesado con éxito"
    }
}
```
> Example response (200):

```json
{
    "status": {
        "code": -1,
        "message": "Correo no encontrado"
    }
}
```

### HTTP Request
`POST api/auth/unlinkDevice`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `$email` | string |  optional  | email del usuario
    
<!-- END_cc5af75259b39025f65ab5d9bfb89e87 -->

#Preguntas Frecuentes


<!-- START_df4bd14828f486d7759e078ae0976d3b -->
## Obtener todas las FAQS

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "127.0.0.0/api/api/getFaqs" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -H "tokenApp: YOUR_API_KEY_HERE"
```

```javascript
const url = new URL(
    "127.0.0.0/api/api/getFaqs"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
    "tokenApp": "YOUR_API_KEY_HERE",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (400):

```json
{
    "status": {
        "code": 400,
        "message": "El token es invalido Tymon\\JWTAuth\\Exceptions\\TokenInvalidException: Wrong number of segments in C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\tymon\\jwt-auth\\src\\Validators\\TokenValidator.php:42\nStack trace:\n#0 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\tymon\\jwt-auth\\src\\Validators\\TokenValidator.php(27): Tymon\\JWTAuth\\Validators\\TokenValidator->validateStructure('{token')\n#1 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\tymon\\jwt-auth\\src\\Token.php(32): Tymon\\JWTAuth\\Validators\\TokenValidator->check('{token')\n#2 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\tymon\\jwt-auth\\src\\JWT.php(304): Tymon\\JWTAuth\\Token->__construct('{token')\n#3 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\tymon\\jwt-auth\\src\\JWT.php(188): Tymon\\JWTAuth\\JWT->setToken('{token')\n#4 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Support\\Facades\\Facade.php(261): Tymon\\JWTAuth\\JWT->parseToken()\n#5 C:\\Users\\USUARIO 1\\Desktop\\identidad\\app\\Http\\Middleware\\JWT.php(12): Illuminate\\Support\\Facades\\Facade::__callStatic('parseToken', Array)\n#6 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): App\\Http\\Middleware\\JWT->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#7 C:\\Users\\USUARIO 1\\Desktop\\identidad\\app\\Http\\Middleware\\Cors.php(24): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#8 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): App\\Http\\Middleware\\Cors->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#9 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\SubstituteBindings.php(41): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#10 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#11 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\ThrottleRequests.php(59): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#12 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Routing\\Middleware\\ThrottleRequests->handle(Object(Illuminate\\Http\\Request), Object(Closure), 60, '1')\n#13 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#14 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#15 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#16 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#17 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#18 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(170): Illuminate\\Routing\\Router->dispatch(Object(Illuminate\\Http\\Request))\n#19 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#20 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#21 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#22 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#23 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#24 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#25 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#26 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#27 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#28 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#29 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#30 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#31 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#32 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#33 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#34 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))\n#35 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))\n#36 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(172): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Illuminate\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)\n#37 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(121): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)\n#38 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(84): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Illuminate\\Routing\\Route), Array, Array)\n#39 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(125): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Illuminate\\Routing\\Route), Array)\n#40 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(69): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)\n#41 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle(Object(Mpociot\\ApiDoc\\Matching\\RouteMatcher))\n#42 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)\n#43 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#44 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#45 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#46 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#47 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(134): Illuminate\\Container\\Container->call(Array)\n#48 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#49 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(121): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#50 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\symfony\\console\\Application.php(1001): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#51 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\symfony\\console\\Application.php(271): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#52 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\symfony\\console\\Application.php(147): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#53 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#54 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#55 C:\\Users\\USUARIO 1\\Desktop\\identidad\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#56 {main}"
    }
}
```

### HTTP Request
`GET api/getFaqs`


<!-- END_df4bd14828f486d7759e078ae0976d3b -->

#general


<!-- START_183015c584096a031bbfb6365be8f70c -->
## api/loginPortal
> Example request:

```bash
curl -X POST \
    "127.0.0.0/api/api/loginPortal" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -H "tokenApp: YOUR_API_KEY_HERE"
```

```javascript
const url = new URL(
    "127.0.0.0/api/api/loginPortal"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
    "tokenApp": "YOUR_API_KEY_HERE",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/loginPortal`


<!-- END_183015c584096a031bbfb6365be8f70c -->

<!-- START_5501444b14e7f508cc1834b8f8fb5b85 -->
## api/auth/getUserPermission
> Example request:

```bash
curl -X GET \
    -G "127.0.0.0/api/api/auth/getUserPermission" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -H "tokenApp: YOUR_API_KEY_HERE"
```

```javascript
const url = new URL(
    "127.0.0.0/api/api/auth/getUserPermission"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
    "tokenApp": "YOUR_API_KEY_HERE",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (400):

```json
{
    "status": {
        "code": 400,
        "message": "El token es invalido Tymon\\JWTAuth\\Exceptions\\TokenInvalidException: Wrong number of segments in C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\tymon\\jwt-auth\\src\\Validators\\TokenValidator.php:42\nStack trace:\n#0 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\tymon\\jwt-auth\\src\\Validators\\TokenValidator.php(27): Tymon\\JWTAuth\\Validators\\TokenValidator->validateStructure('{token')\n#1 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\tymon\\jwt-auth\\src\\Token.php(32): Tymon\\JWTAuth\\Validators\\TokenValidator->check('{token')\n#2 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\tymon\\jwt-auth\\src\\JWT.php(304): Tymon\\JWTAuth\\Token->__construct('{token')\n#3 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\tymon\\jwt-auth\\src\\JWT.php(188): Tymon\\JWTAuth\\JWT->setToken('{token')\n#4 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Support\\Facades\\Facade.php(261): Tymon\\JWTAuth\\JWT->parseToken()\n#5 C:\\Users\\USUARIO 1\\Desktop\\identidad\\app\\Http\\Middleware\\JWT.php(12): Illuminate\\Support\\Facades\\Facade::__callStatic('parseToken', Array)\n#6 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): App\\Http\\Middleware\\JWT->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#7 C:\\Users\\USUARIO 1\\Desktop\\identidad\\app\\Http\\Middleware\\Cors.php(24): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#8 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): App\\Http\\Middleware\\Cors->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#9 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\SubstituteBindings.php(41): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#10 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#11 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\ThrottleRequests.php(59): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#12 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Routing\\Middleware\\ThrottleRequests->handle(Object(Illuminate\\Http\\Request), Object(Closure), 60, '1')\n#13 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#14 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#15 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#16 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#17 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#18 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(170): Illuminate\\Routing\\Router->dispatch(Object(Illuminate\\Http\\Request))\n#19 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#20 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#21 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#22 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#23 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#24 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#25 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#26 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#27 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#28 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#29 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#30 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#31 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#32 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#33 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#34 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))\n#35 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))\n#36 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(172): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Illuminate\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)\n#37 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(121): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)\n#38 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(84): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Illuminate\\Routing\\Route), Array, Array)\n#39 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(125): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Illuminate\\Routing\\Route), Array)\n#40 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(69): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)\n#41 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle(Object(Mpociot\\ApiDoc\\Matching\\RouteMatcher))\n#42 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)\n#43 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#44 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#45 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#46 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#47 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(134): Illuminate\\Container\\Container->call(Array)\n#48 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#49 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(121): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#50 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\symfony\\console\\Application.php(1001): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#51 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\symfony\\console\\Application.php(271): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#52 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\symfony\\console\\Application.php(147): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#53 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#54 C:\\Users\\USUARIO 1\\Desktop\\identidad\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#55 C:\\Users\\USUARIO 1\\Desktop\\identidad\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#56 {main}"
    }
}
```

### HTTP Request
`GET api/auth/getUserPermission`


<!-- END_5501444b14e7f508cc1834b8f8fb5b85 -->


