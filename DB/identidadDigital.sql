USE [master]
GO
/****** Object:  Database [identidad]    Script Date: 4/05/2020 8:13:14 p. m. ******/
CREATE DATABASE [identidad]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'identidad', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLEXPRESS\MSSQL\DATA\identidad.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'identidad_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLEXPRESS\MSSQL\DATA\identidad_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [identidad] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [identidad].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [identidad] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [identidad] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [identidad] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [identidad] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [identidad] SET ARITHABORT OFF 
GO
ALTER DATABASE [identidad] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [identidad] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [identidad] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [identidad] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [identidad] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [identidad] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [identidad] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [identidad] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [identidad] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [identidad] SET  DISABLE_BROKER 
GO
ALTER DATABASE [identidad] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [identidad] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [identidad] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [identidad] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [identidad] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [identidad] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [identidad] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [identidad] SET RECOVERY FULL 
GO
ALTER DATABASE [identidad] SET  MULTI_USER 
GO
ALTER DATABASE [identidad] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [identidad] SET DB_CHAINING OFF 
GO
ALTER DATABASE [identidad] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [identidad] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [identidad] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [identidad] SET QUERY_STORE = OFF
GO
USE [identidad]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [identidad]
GO
/****** Object:  Table [dbo].[admin]    Script Date: 4/05/2020 8:13:15 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[admin](
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[email] [varchar](50) NOT NULL,
	[status] [varchar](50) NULL,
	[updated_at] [datetime] NOT NULL,
	[created_at] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[faqs]    Script Date: 4/05/2020 8:13:15 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[faqs](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](50) NULL,
	[content] [nvarchar](max) NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[logs]    Script Date: 4/05/2020 8:13:15 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[logs](
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[code] [varchar](50) NULL,
	[action] [varchar](50) NULL,
	[message] [text] NULL,
	[systenMessage] [text] NULL,
	[dateLog] [datetime] NULL,
	[updated_at] [datetime] NOT NULL,
	[created_at] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_phone]    Script Date: 4/05/2020 8:13:15 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_phone](
	[user_id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[user_email_un] [varchar](50) NULL,
	[user_email_ext] [varchar](50) NULL,
	[user_uid] [varchar](50) NOT NULL,
	[user_phone_model] [varchar](50) NULL,
	[user_phone_brand] [varchar](50) NULL,
	[user_last_access] [datetime] NULL,
	[updated_at] [datetime] NOT NULL,
	[created_at] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[users]    Script Date: 4/05/2020 8:13:15 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[users](
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[code] [varchar](50) NOT NULL,
	[email] [varchar](50) NOT NULL,
	[password] [nvarchar](max) NOT NULL,
	[token] [varchar](250) NULL,
	[status] [varchar](50) NULL,
	[image] [nvarchar](max) NULL,
	[created_at] [datetime] NOT NULL,
	[updated_at] [datetime] NOT NULL,
 CONSTRAINT [PK_users_B51D3DEAB209AB8E] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[faqs] ADD  DEFAULT (sysdatetime()) FOR [created_at]
GO
ALTER TABLE [dbo].[faqs] ADD  DEFAULT (sysdatetime()) FOR [updated_at]
GO
ALTER TABLE [dbo].[user_phone] ADD  CONSTRAINT [DF_user_phone_updated_at]  DEFAULT (sysdatetime()) FOR [updated_at]
GO
ALTER TABLE [dbo].[user_phone] ADD  CONSTRAINT [DF_user_phone_created_at]  DEFAULT (sysdatetime()) FOR [created_at]
GO
ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF_users_created_at]  DEFAULT (sysdatetime()) FOR [created_at]
GO
ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF_users_updated_at]  DEFAULT (sysdatetime()) FOR [updated_at]
GO
USE [master]
GO
ALTER DATABASE [identidad] SET  READ_WRITE 
GO
